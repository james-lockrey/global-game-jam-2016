﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using Random = UnityEngine.Random;

public class BarManager : MonoBehaviour
{
    private static BarManager instance = null;
    public static BarManager Instance { get { return instance; } }

    [Header("Menu")]
    [SerializeField]
    private List<Recipe> menu = new List<Recipe>();
    [SerializeField]
    private Recipe debugRecipe;

    [Header("Earnings")]
    [SerializeField]
    private Text earningsText;

    private Recipe currentOrder;

    [Header("MiniGameOrbs")]
    public List<MiniGameOrb> miniGameOrbs = new List<MiniGameOrb>();
    private List<MiniGameOrb> chosenOrbs = new List<MiniGameOrb>();
    public Button beginButton;

    [Header("Customer")]
    public Customer customer;

    [Space(10)]
    [SerializeField]
    private SequenceContainer currentSequence;
    private bool isSequenceCorrect;

	[Header("Sounds")]
	public SoundModule kachingSounds;

    public delegate void NewRecipeDelegate(Recipe newRecipe);
    public static event NewRecipeDelegate OnNewRecipe = delegate { };
    public delegate void SequenceSelectedDelegate(List<MiniGame> sequence);
    public static event SequenceSelectedDelegate OnSequenceStarted = delegate { };

    #region Properties
    public List<Recipe> Menu { get { return menu; } }
    public int TotalEarnings
    {
        get { return Core.Instance.SaveManager.TotalEarnings; }
        set
        {
            Core.Instance.SaveManager.TotalEarnings = value;
            earningsText.text = value.ToString();
        }
    }
    #endregion

    #region MonoBehaviour Lifecycle
    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        else
        {
            instance = this;
        }

        //menu = menu.OrderBy(x => x.value);
        miniGameOrbs.ForEach(x => x.gameObject.SetActive(false));
        beginButton.gameObject.SetActive(false);

        GameManager.OnGameStart += OnGameStart;
        SequenceManager.OnSequenceComplete += OnSequenceComplete;
    }
    #endregion

    #region Events
    private void OnGameStart()
    {
        gameObject.SetActive(true);
        StartCoroutine(CR_OnGameStart());
    } 

    private void OnSequenceComplete(float accuracyPercentage)
    {
		gameObject.SetActive(true);

		TotalEarnings += (int)(accuracyPercentage * 100f);
		StartCoroutine(CR_OnSequenceComplete());
    }
    #endregion

    private IEnumerator CR_OnGameStart()
    {
        // Spawn the customer
        customer.Spawn();
        yield return new WaitForSeconds(1f);
        // Generate new recipe
        currentOrder = GetNewOrder();
        customer.RequestOrder(currentOrder);
        yield return new WaitForSeconds(2f);

        // Send out new recipe event
        OnNewRecipe(currentOrder);

        // Spawn orbs one after the other
        chosenOrbs.Clear();
        List<MiniGame> alreadySpawned = new List<MiniGame>();
        alreadySpawned.AddRange(currentOrder.Sequence);
        int i = 0;
        while(alreadySpawned.Count > 0)
        {
            int rIndex = Random.Range(0, alreadySpawned.Count);
            miniGameOrbs[i].Spawn(alreadySpawned[rIndex]);
            chosenOrbs.Add(miniGameOrbs[i]);

            alreadySpawned.RemoveAt(rIndex);

            i++;

            yield return new WaitForSeconds(Random.Range(0.1f, 0.35f));
        }
    }

	private IEnumerator CR_OnSequenceComplete()
	{
		if(kachingSounds != null)
			SoundKit.instance.playOneShot(kachingSounds.Clip);
		
		// Show dish on counter
		customer.ServeFood(currentOrder, isSequenceCorrect);
		
		// Customer eats dish
		while(customer.IsEating)
			yield return null;
		
		// Customer disappears
		customer.gameObject.SetActive(false);
		yield return new WaitForSeconds(1f);
		customer.gameObject.SetActive(true);

		// Start again
		OnGameStart ();
	}

    public void StartSequence()
    {
        beginButton.gameObject.SetActive(false);

        StartCoroutine(CR_StartSequence());
    }

	private IEnumerator CR_StartSequence() 
    {
        yield return new WaitForSeconds(0.1f);

        // Show for 
        int i = 0;
        foreach(SequenceSlot orb in currentSequence.sequenceSlots) 
        {
            if (i >= currentOrder.Sequence.Count) 
            {
                continue;
            }

            Image image = orb.transform.GetChild(0).GetComponent<Image>();
            image.color = (orb.linkedTo == currentOrder.Sequence[i]) ? Color.green : Color.red;
            i++;

            yield return new WaitForSeconds(0.2f);
        }

        yield return new WaitForSeconds(1.0f);

        foreach (SequenceSlot orb in currentSequence.sequenceSlots) 
        {
            Image image = orb.transform.GetChild(0).GetComponent<Image>();
            image.color = Color.white;
        }

        List<MiniGame> sequence = currentSequence.GetSequence();
        isSequenceCorrect = true;
        for(int j = 0; j < sequence.Count; j++)
        {
            if(sequence[j] != currentOrder.Sequence[j])
            {
                isSequenceCorrect = false;
                break;
            }
        }

        OnSequenceStarted(sequence);

        chosenOrbs.Clear();
        gameObject.SetActive(false);
    }

    public void SelectMiniGame(MiniGame mg)
    {
        currentSequence.Add(mg);

        if(currentSequence.Length == chosenOrbs.Count)
        {
            // Enable the sushi button
            beginButton.gameObject.SetActive(true);
        }
    }

    public void RemoveMiniGame(MiniGame mg)
    {
        // Remove it from the sequence
        currentSequence.Remove(mg);
        // Show the orb
        chosenOrbs.Where(x => x.linkedTo == mg).FirstOrDefault().Spawn(mg);
        // Disable the button
        beginButton.gameObject.SetActive(false);
    }

    public Recipe GetNewOrder()
    {
        if (debugRecipe != null)
            return debugRecipe;

        return menu[Random.Range(0, menu.Count)];
    }
}
