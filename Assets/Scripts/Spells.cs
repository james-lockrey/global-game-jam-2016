﻿using UnityEngine;
using System.Collections;

public class Spells : MonoBehaviour {
	//Set number of touches needed to complete gesture
	public int numOfTouches;
	//Number of checkPoints to complete gesture
	public int numOfCheckPoints;

	//CheckPoints the user needs to touch in order to pass
	public struct checkPoint{
		public checkPoint(Vector2 posA, Vector2 posB, Vector2 cen, float aminX, float aminY, float amaxX, float amaxY, bool checkBool){
			positionA = posA;
			positionB = posB;
			center = cen;
			maxX = amaxX;
			maxY = amaxY;
			minX = aminX;
			minY = aminY;
			check = checkBool;
		}

		//Position of the 2 points making the checkpoint
		public Vector2 positionA;
		public Vector2 positionB;
		public Vector2 center;
		public float minX;
		public float minY;
		public float maxX;
		public float maxY;
		//Whether it has been checked or not
		public bool check;
	}

	public Transform[] myCheckPointPosA;
	public Transform[] myCheckPointPosB;
	public checkPoint[] myCheckPoints;
	public bool wrongGesture;

	//Name of the next scene (next step in spell sequence)
	public Spells nextStep;
	//Current sequence number
	public int currSequence;
	//Total in sequence
	public int totalInSequence;
    //finishedGestureCorrectly
    public bool correctGesture = false;

	public void Start(){
		
		myCheckPoints = new checkPoint[numOfCheckPoints];
		if (numOfCheckPoints > 0) {
			for (int i = 0; i < numOfCheckPoints; i++) {
				myCheckPoints [i].positionA.Set(myCheckPointPosA [i].position.x, myCheckPointPosA[i].position.y);
				myCheckPoints [i].positionB.Set(myCheckPointPosB [i].position.x, myCheckPointPosB[i].position.y);
				myCheckPoints [i].check = false;
			}
		}
		wrongGesture = false;
	}

	public void CheckGesture(){
		foreach (checkPoint checkPt in myCheckPoints) {
			if (checkPt.check == true) {
				continue;
			} else if (checkPt.check == false) {
				wrongGesture = true;
				break;
			}
		}
		if (wrongGesture == true) {
			//User input was incorrect
			Debug.Log ("wrong gesture");
			correctGesture = true;
		} else {
			//Succeeded
			Debug.Log ("correct gesture");
			if (currSequence < totalInSequence) {
                //Application.LoadLevel (nextStep);
                //Move to the next step
                correctGesture = true;
            } else {
				//Spell complete
			}
		}
	}
}
