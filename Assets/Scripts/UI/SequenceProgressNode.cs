﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

public class SequenceProgressNode : MonoBehaviour
{
    // CACHED COMPONENTS
    private Image _image;

    private static SequenceProgressNode active = null;

    private static Action OnActivate = delegate { };

    #region MonoBehaviour Lifecycle
    void Awake()
    {
        _image = GetComponent<Image>();

        _image.sprite = null;

        OnActivate += CheckActive;
    }
    #endregion

    public void SetMiniGame(MiniGame target)
    {
        if (target != null)
        {
            // Show the ingredient sprite
            _image.sprite = UIManager.GetIngredientSprite(target.baseIngredient);
            _image.enabled = true;
        }
    }

    public void SetActive()
    {
        active = this;
        transform.localScale = Vector3.one * 1.75f;
        OnActivate();
    }

    private void CheckActive()
    {
        if(this != active)
        {
            transform.localScale = Vector3.one;
        }
    }
}
