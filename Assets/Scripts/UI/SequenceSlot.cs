﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SequenceSlot : MonoBehaviour
{
    // CACHED COMPONENTS
    private Button _button;
    private Image _image;

    public MiniGame linkedTo;

    #region MonoBehaviour Lifecycle
    void Awake()
    {
        _button = GetComponent<Button>();
        _image = GetComponent<Image>();

        _image.enabled = false;
    }

    void OnEnable()
    {

    }

    void Start()
    {

    }

    void OnDisable()
    {

    }
    #endregion

    public void SetMiniGame(MiniGame target)
    {
        Clear();

        if (target != null)
        {
            // Add the click-to-remove function
            _button.onClick.AddListener(RemoveIngredient);
            // Show the ingredient sprite
            _image.sprite = UIManager.GetIngredientSprite(target.baseIngredient);
            _image.enabled = true;
        }

        linkedTo = target;
    }

    public void RemoveIngredient()
    {
        if (linkedTo != null)
        {
            Clear();
            // Notify the Bar
            BarManager.Instance.RemoveMiniGame(linkedTo);
        }
    }

    public void Clear()
    {
        // Remove the click-to-remove function
        _button.onClick.RemoveAllListeners();
        // Remove and disable the ingredient sprite
        _image.sprite = null;
        _image.enabled = false;
    }

    public void Disable()
    {
        gameObject.SetActive(false);
    }
}
