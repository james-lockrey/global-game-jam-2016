﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    private static UIManager instance = null;
    public static UIManager Instance { get { return instance; } }

    public GameObject barBackground;

    [Header("Screens")]
    [SerializeField]
    private CanvasGroup homeScreen;
    [SerializeField]
    private CanvasGroup barScreen;
    [SerializeField]
    private CanvasGroup sequenceScreen;
    
    [Serializable]
    public class IngredientSprite
    {
        public Ingredient ingredient;
        public Sprite sprite;
    }
    [Header("Ingredient Map")]
    public List<IngredientSprite> ingredientSprites = new List<IngredientSprite>();
    private static Dictionary<Ingredient, Sprite> ingredientMap = new Dictionary<Ingredient, Sprite>();

	[Header("Sounds")]
	public AudioClip startSound;
	public AudioClip wizushiSound;

	#region Properties
	public float RelativeAspect { get; private set; }
	#endregion

    #region MonoBehaviour Lifecycle
    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        else
        {
            instance = this;
        }

        foreach (IngredientSprite ingSpr in ingredientSprites)
            ingredientMap.Add(ingSpr.ingredient, ingSpr.sprite);
		
		// What's the aspect relative to the designed aspect?
		RelativeAspect = Camera.main.aspect * (10f / 16f);

        GameManager.OnGameStart += OnGameStart;
        GameManager.OnGameExit += OnGameExit;
        BarManager.OnSequenceStarted += OnSequenceSelected;
        SequenceManager.OnSequenceComplete += OnSequenceComplete;
    }

    void Start()
    {
        barBackground.SetActive(true);
        GoToHome();
    }
    #endregion

    #region Events
    private void OnGameStart()
    {
        GoToBar();
    }

    private void OnGameExit()
    {
        GoToHome();
    }

    private void OnSequenceSelected(List<MiniGame> sequence)
    {
        barBackground.SetActive(false);

		if(wizushiSound != null)
			SoundKit.instance.playOneShot(wizushiSound);
    }

    private void OnSequenceComplete(float accuracyPercentage)
    {
        barBackground.SetActive(true);
    }
    #endregion

    public void GoToBar()
    {
        barScreen.gameObject.SetActive(true);
        homeScreen.gameObject.SetActive(false);
        sequenceScreen.gameObject.SetActive(false);

		if(startSound != null)
			SoundKit.instance.playOneShot(startSound);
    }

    public void GoToHome()
    {
        homeScreen.gameObject.SetActive(true);
        barScreen.gameObject.SetActive(false);
        sequenceScreen.gameObject.SetActive(false);
    }

    public static Sprite GetIngredientSprite(Ingredient ingredient)
    {
        return ingredientMap[ingredient];
    }

    private IEnumerator CR_LerpAlpha(CanvasGroup target, float fromAlpha, float toAlpha, float overSeconds = 0.6f)
    {
        for (float t = 0f; t < 1f; t += Time.deltaTime * 1f / overSeconds)
        {
            target.alpha = Mathf.Lerp(fromAlpha, toAlpha, t);
            yield return null;
        }

        target.alpha = toAlpha;
    }
}
