﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class SequenceProgress : MonoBehaviour
{
    private List<SequenceProgressNode> sequenceProgressNodes = new List<SequenceProgressNode>();

    #region MonoBehaviour Lifecycle
    void Awake()
    {
        sequenceProgressNodes = GetComponentsInChildren<SequenceProgressNode>().ToList();

        BarManager.OnSequenceStarted += OnSequenceStarted;
        SequenceManager.OnMiniGameComplete += OnMiniGameComplete;
    }
    #endregion

    #region Events
    private void OnSequenceStarted(List<MiniGame> sequence)
    {
        // Setup the active icons
        for (int i = 0; i < sequence.Count; i++)
        {
            sequenceProgressNodes[i].gameObject.SetActive(true);
            sequenceProgressNodes[i].SetMiniGame(sequence[i]);
        }

        // Turn off the unused icons
        for(int i = sequence.Count; i < sequenceProgressNodes.Count; i++)
        {
            sequenceProgressNodes[i].gameObject.SetActive(false);
        }

        sequenceProgressNodes[0].SetActive();
    }

    private void OnMiniGameComplete(MiniGame completed)
    {
        sequenceProgressNodes[SequenceManager.Instance.SequencePosition].SetActive();
    }
    #endregion
}
