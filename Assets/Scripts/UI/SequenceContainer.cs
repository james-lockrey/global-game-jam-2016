﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class SequenceContainer : MonoBehaviour
{
    public List<SequenceSlot> sequenceSlots = new List<SequenceSlot>();
    private int sequenceLength = 0;

	[Header("Sounds")]
	public SoundModule tapSounds;

    #region Properties
    public int Length { get { return sequenceLength; } }
    #endregion

    #region MonoBehaviour Lifecycle
    void Awake()
    {
        foreach (Transform child in transform)
            sequenceSlots.Add(child.GetComponent<SequenceSlot>());

        sequenceSlots.ForEach(x => x.gameObject.SetActive(false));

        BarManager.OnNewRecipe += OnNewRecipe;
        BarManager.OnSequenceStarted += OnSequenceStarted;
    }

    void Start()
    {

    }
    #endregion

    #region Events
    private void OnNewRecipe(Recipe newRecipe)
    {
        // Clear all the slots
        for(int i = 0; i < sequenceSlots.Count; i++)
        {
            sequenceSlots[i].gameObject.SetActive(true);
            sequenceSlots[i].Clear();
        }

        // Disable the unneeded slots
        for(int i = newRecipe.Length; i < sequenceSlots.Count; i++)
        {
            sequenceSlots[i].Disable();
        }
    }

    private void OnSequenceStarted(List<MiniGame> sequence)
    {
        for(int i = 0; i < sequenceSlots.Count; i++)
        {
            sequenceSlots[i].Clear();
            sequenceSlots[i].gameObject.SetActive(false);
        }

        sequenceLength = 0;
    }
    #endregion

    public void Add(MiniGame newMiniGame)
    {
        if (sequenceLength <= sequenceSlots.Count - 1)
        {
            sequenceSlots[sequenceLength].SetMiniGame(newMiniGame);
            sequenceLength++;
        }

		if(tapSounds != null)
			SoundKit.instance.playOneShot(tapSounds.Clip);
    }

    public void Remove(MiniGame miniGame)
    {
        for(int i = 0; i < sequenceSlots.Count; i++)
        {
            if(sequenceSlots[i].linkedTo == miniGame)
            {
                for(int j = i; j < sequenceSlots.Count; j++)
                {
                    if(j == sequenceSlots.Count - 1) 
                    {
                        sequenceSlots[j].SetMiniGame(null);
                    } 
                    else 
                    {
                        sequenceSlots[j].SetMiniGame(sequenceSlots[j + 1].linkedTo);
                    }
                }

                break;
            }
        }

        sequenceLength--;
    }

    public List<MiniGame> GetSequence()
    {
        List<MiniGame> sequence = new List<MiniGame>();
        for (int i = 0; i < sequenceLength; i++)
        {
            sequence.Add(sequenceSlots[i].linkedTo);
        }

        return sequence;
    }
}
