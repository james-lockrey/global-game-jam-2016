﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CanvasGroup))]
public class Modal : MonoBehaviour
{
    // CACHED COMPONENTS
    private CanvasGroup _canvasGroup;

    public float fadeDuration = 0.15f;

    [Space(10)]
    public AudioClip openSound;
    public AudioClip closeSound;

    #region MonoBehaviour Lifecycle
    void Awake()
    {
        _canvasGroup = GetComponent<CanvasGroup>();
        _canvasGroup.alpha = 0;

        gameObject.SetActive(false);
    }
    #endregion

    public void Open()
    {
        gameObject.SetActive(true);
        _canvasGroup.alpha = 0;
        StartCoroutine(CR_Fade(1f, true));
        //SoundKit.instance.playOneShot(openSound, 0.75f);
    }

    public void Close()
    {
        StartCoroutine(CR_Fade(0f, false));
        //SoundKit.instance.playOneShot(closeSound, 0.75f);
    }

    public IEnumerator CR_Fade(float targetAlpha, bool endEnabled)
    {
        float startAlpha = _canvasGroup.alpha;

        for (float t = 0f; t < 1f; t += (Time.deltaTime / fadeDuration) * Time.timeScale)
        {
            _canvasGroup.alpha = Mathf.Lerp(startAlpha, targetAlpha, t);
            yield return null;
        }

        gameObject.SetActive(endEnabled);
        _canvasGroup.alpha = targetAlpha;
    }
}
