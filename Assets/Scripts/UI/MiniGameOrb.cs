﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MiniGameOrb : MonoBehaviour
{
    // CACHED COMPONENTS
    private Button _button;
    private Image _image;

    [HideInInspector]
    public MiniGame linkedTo;

    #region MonoBehaviour Lifecycle
    void Awake()
    {
        _button = GetComponent<Button>();
        _image = GetComponent<Image>();
    }
    #endregion

    public void Spawn(MiniGame mg)
    {
        // Enable
        gameObject.SetActive(true);
        // Link to mini-game and show ingredient icon
        linkedTo = mg;
        _image.sprite = UIManager.GetIngredientSprite(linkedTo.baseIngredient);
        // Add button click events
        _button.interactable = true;
    }

    public void OnTap()
    {
        if (_button.interactable)
        {
            _button.interactable = false;
            gameObject.SetActive(false);
            BarManager.Instance.SelectMiniGame(linkedTo);
        }
    }
}
