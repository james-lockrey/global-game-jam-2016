﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Recipe : MonoBehaviour
{
    public Sprite ExampleSprite;
	public Sprite FailedSprite;

    public List<MiniGame> Sequence = new List<MiniGame>();
    public int Value = 10;

    public int Length { get { return Sequence.Count; } }
}
