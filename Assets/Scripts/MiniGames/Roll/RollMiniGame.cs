﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class RollMiniGame : MiniGame
{
    [Space(10)]
    public float slowdown = 8;
    public float finishThreshold = 5.1f;

    public GameObject roll;
    public BoxCollider rollLeftCollider;
    public BoxCollider rollRightCollider;
    public Image seaweedImage;
    public Image riceSeaweedImage;
    public float maxSeaweedHeight;
    public float width = 10;
    public float maxAngle = Mathf.PI / 8;

    float rollMinY;
    new Camera camera;
    Vector3 defaultPos;

    bool lastOneTouchDown;
    int leftTouchId = -1;
    int rightTouchId = -1;
    Vector2 leftTouchPos;
    Vector2 rightTouchPos;
    Vector3 leftWorldPos;
    Vector3 rightWorldPos;

    Vector3 lastRollPos;
    Vector3 rollVel;

    float riceSeaweedMinFill;

    #region MonoBehaviour Lifecycle
    protected override void Awake()
    {
        base.Awake();

        camera = Camera.main;
    }

    protected override void Start()
    {
		base.Start ();

        defaultPos = transform.position;

        leftTouchPos = camera.WorldToScreenPoint(rollLeftCollider.transform.TransformPoint(rollLeftCollider.center));
        rightTouchPos = camera.WorldToScreenPoint(rollRightCollider.transform.TransformPoint(rollRightCollider.center));
        rollMinY = roll.transform.position.y - 0.2f * transform.lossyScale.y;

        riceSeaweedMinFill = riceSeaweedImage.fillAmount;
    }

    protected override void Update()
    {
        base.Update();

        foreach (Touch touch in Input.touches)
        {
            // Track touches left in roll colliders
            if (touch.phase == TouchPhase.Began)
            {
                Ray ray = camera.ScreenPointToRay(new Vector3(touch.position.x, touch.position.y, 0));
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit, Mathf.Infinity))
                {
                    if (hit.collider == rollLeftCollider)
                    {
                        leftTouchId = touch.fingerId;
                        Debug.Log("set Left");
                    }
                    else if (hit.collider == rollRightCollider)
                    {
                        rightTouchId = touch.fingerId;
                        Debug.Log("set Right");
                    }
                }
            }
            if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled)
            {
                if (touch.fingerId == leftTouchId)
                {
                    leftTouchId = -1;
                }
                else if (touch.fingerId == rightTouchId)
                {
                    rightTouchId = -1;
                }
            }

            // Update touch positions
            if (touch.fingerId == leftTouchId)
            {
                leftTouchPos = touch.position;
                //Debug.Log("left " + leftTouchPos);
            }
            if (touch.fingerId == rightTouchId)
            {
                rightTouchPos = touch.position;
                //Debug.Log("right " + rightTouchPos);
            }
        }

        bool oneTouchDown = leftTouchId != -1 || rightTouchId != -1;

        // Calc world positions
        RaycastHit ahit;
        if (Physics.Raycast(camera.ScreenPointToRay(leftTouchPos), out ahit, Mathf.Infinity))
        {
            leftWorldPos = ahit.point;
        }
        if (Physics.Raycast(camera.ScreenPointToRay(rightTouchPos), out ahit, Mathf.Infinity))
        {
            rightWorldPos = ahit.point;
        }

        Debug.DrawLine(leftWorldPos, rightWorldPos, Color.red);

        if (oneTouchDown)
        {
            // Now calculate the lowest possible position
            float ymin = Mathf.Min(leftWorldPos.y, rightWorldPos.y);

            // Calc angle & apply limits
            float diff = (rightWorldPos.y - leftWorldPos.y) / (rightWorldPos.x - leftWorldPos.x);
            float angle = diff == 0 ? 0 : Mathf.Clamp(Mathf.Atan(diff), -Mathf.PI / 8, Mathf.PI / 8);
            float ydiff = (width / 2) * Mathf.Sin(angle);

            roll.transform.position = new Vector3(roll.transform.position.x, ymin + ydiff / 2, roll.transform.position.z);
            roll.transform.rotation = Quaternion.Euler(0, 0, angle * Mathf.Rad2Deg);

            // Update velocity
            float alpha = 0.9f;
            //Debug.Log((roll.transform.position - lastRollPos));
            rollVel = (1 - alpha) * rollVel + alpha * ((roll.transform.position - lastRollPos) / Time.deltaTime);
            rollVel.y = Mathf.Clamp(rollVel.y, -1, 1);
            rollVel.x = 0;
        }
        else if (!oneTouchDown && lastOneTouchDown)
        {
            Debug.Log(rollVel);
        }
        else if (!oneTouchDown)
        {
            rollVel = Vector3.Lerp(rollVel, Vector3.zero, Time.deltaTime * slowdown);
            roll.transform.position += rollVel;
        }

        if(leftTouchId != -1 && rightTouchId != -1) 
        {
            PerformedFirstAction = true;
        }

        // Update mask
        float seaweedBot = seaweedImage.transform.TransformPoint(0, seaweedImage.rectTransform.rect.yMin, 0).y;
        float seaweedTop = seaweedImage.transform.TransformPoint(0, seaweedImage.rectTransform.rect.yMax, 0).y;
        float maskDiff = ((roll.transform.position.y - seaweedBot) / (seaweedTop - seaweedBot));
        seaweedImage.fillAmount = 1 - maskDiff;
        riceSeaweedImage.fillAmount = Mathf.Lerp(riceSeaweedMinFill, 1, maskDiff);

        // Clamp pos
        roll.transform.position = new Vector3(roll.transform.position.x, Mathf.Max(rollMinY, roll.transform.position.y), roll.transform.position.z);
        lastRollPos = roll.transform.position;
        lastOneTouchDown = oneTouchDown;
    }
    #endregion

    #region Inherited Functions
    protected override float CalculateScore()
    {
        return 1.0f - roll.transform.rotation.eulerAngles.z / maxAngle;
    }
    #endregion
}
