﻿using UnityEngine;
using System.Collections;

public enum Ingredient
{
    Seaweed,
    Rice,
    Salmon,
    Cucumber,
    Zombie,
    Griffin,
    Unicorn,
    Newt,
    Pixie,
    Dragon,
    Mermaid,
    Ghost,
    Basilisk,
    Mushroom,
    Demon,
    Vampire
}

public abstract class MiniGame : MonoBehaviour
{
    // CACHED COMPONENTS
    protected Collider2D _collider2D;

    public Ingredient baseIngredient;
    public string description;

    public float timeLimit = 6f;
    private float timeRemaining;

    protected float currentNormalizedScore = 0f;

    #region Properties
    public Collider2D Collider2D { get { return _collider2D; } }
    public float TimeRemaining { get { return timeRemaining; } }
    public float PercentageTimeRemaining { get { return Mathf.Clamp01(timeRemaining / timeLimit); } }
    public bool PerformedFirstAction { get; protected set; }
    #endregion

    #region MonoBehaviour Lifecycle
    protected virtual void Awake()
    {
        _collider2D = GetComponent<Collider2D>();

        timeRemaining = timeLimit;

		if (UIManager.Instance != null)
		{
			// Scale size by the difference between designed / actual aspect ratio
			transform.localScale = new Vector3 (UIManager.Instance.RelativeAspect, 1f, 1f);
		}

		SequenceManager.OnMiniGameComplete += OnMiniGameComplete;
		SequenceManager.OnNewMiniGameReady += OnNewMiniGameReady;

        // Make sure rigid bodies dont spaz out
        foreach (Rigidbody body in GetComponentsInChildren<Rigidbody>()) {
            body.Sleep();
        }
        foreach (Rigidbody2D body in GetComponentsInChildren<Rigidbody2D>()) {
            body.Sleep();
        }
        _collider2D.isTrigger = true;
    }

	protected virtual void Start()
	{
	}

    protected virtual void OnDestroy()
    {
        SequenceManager.OnMiniGameComplete -= OnMiniGameComplete;
        SequenceManager.OnNewMiniGameReady -= OnNewMiniGameReady;
    }

    protected virtual void Update()
    {
        if (timeRemaining >= 0f)
        {
            timeRemaining -= Time.deltaTime;
            currentNormalizedScore = Mathf.Clamp01(CalculateScore());
        }
        else
        {
			if (SequenceManager.Instance) 
			{
				SequenceManager.Instance.FireMiniGameComplete(this, 0f, currentNormalizedScore);
			}

            enabled = false;
        }
    }
    #endregion

    #region Events
    protected virtual void OnNewMiniGameReady(MiniGame current)
    {
        if(this != current)
        {
            gameObject.SetActive(false);
        }
        else
        {
            enabled = true;
            timeRemaining = timeLimit;
        }
    }

    protected virtual void OnMiniGameComplete(MiniGame completed)
    {

    }
    #endregion

    protected abstract float CalculateScore();
}
