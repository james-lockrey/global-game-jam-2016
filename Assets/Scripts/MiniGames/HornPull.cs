﻿using UnityEngine;
using System.Collections;

public class HornPull : MonoBehaviour  {
	//If the pull was successful
    public bool Pulled { get; protected set; }
	private bool hornsTouched;
	private Vector2 direction;
	//Horn's original scale
	private Vector3 hornScale;
	//Timer for horn stretch
	private float hornTimer;
	private float hornTime = 0.5f;
	//If the horn's have finished their scaling up/down effect
	private bool scaled;
	//If dragon scales are being pulled
	private bool dragonScalePull;
	private float scaleMoveSpeed = 0.005f;
	//X and Y bounds to collect the scales
	private float xBound = 4.5f;
	private float yBound = 4f;
	//Border bounds
	private float borderX = 7.8f;
	private float borderY = 4.8f;

	private Renderer rendererObject = null;
	private Color c;

	// Use this for initialization
	void Start () {
		hornScale = transform.lossyScale;
		rendererObject = GetComponent<SpriteRenderer> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (hornsTouched) {
			//Stretch the horns
			if (hornTimer <= hornTime) {
				scaled = false;
				gameObject.transform.localScale.Set (1, 5, 1);
				hornTimer += Time.deltaTime;
			} else {
				hornTimer = 0;
				scaled = true;
				hornsTouched = false;
			}
		}
		if (Pulled) {
			if (scaled) {
				if (hornTimer <= hornTime) {
					transform.Translate (new Vector3 (direction.x, direction.y, 0) * Time.deltaTime);
					hornTimer += Time.deltaTime;
				} else {
					Pulled = false;
				}
				scaled = false;
			}
			if (dragonScalePull) {
				Debug.Log (transform.position);
				//If it's still not out of the area
				if (Mathf.Abs(transform.position.x) <= xBound && Mathf.Abs(transform.position.y) <= yBound) {
					//keep translating the scale
					transform.Translate (new Vector3 (direction.x, direction.y, 0) * scaleMoveSpeed);
				} else if (Mathf.Abs(transform.position.x) > xBound || Mathf.Abs(transform.position.y) >= yBound) {
					//Stop moving
					Debug.Log("stop moving");
					transform.Translate(new Vector3(0,0,0));
					if (transform.position.x > borderX) {
						transform.position = new Vector3 (borderX, transform.position.y, transform.position.z);
					}
					if (transform.position.y > borderY) {
						transform.position = new Vector3 (transform.position.x, borderY, transform.position.z);
					}
				}
				else {
					Pulled = false;
					dragonScalePull = false;
				}
			}
		}
	}

	public void HornTouched(){
		hornTimer = 0;
		hornsTouched = true;
	}

	public void HornTouchEnded(Vector2 deltaPos){
		direction = deltaPos;
		//If the direction was correct
		if (deltaPos.y > 0) {
			Pulled = true;
		}
	}

	public void ScaleTouched(){
	}

	public void ScaleTouchEnded(Vector2 deltaPos){
		direction = deltaPos;
		Pulled = true;
		dragonScalePull = true;
	}
}
