﻿using UnityEngine;
using System.Collections;
using System;

public class GrindMiniGame : MiniGame 
{
    [Space(10)]
    public Turner turner;

    float maxDegrees;

    #region MonoBehaviour Lifecycle
    protected override void Start()
    {
		base.Start();

        maxDegrees = turner.maxDegrees;        
    }

    protected override void Update() 
    {
        base.Update();

        if(turner.maxDegrees != maxDegrees) 
        {
            PerformedFirstAction = true;
        }
    }
    #endregion

    #region Inherited Functions
    protected override float CalculateScore()
    {
        return 1.0f - (turner.maxDegrees) / maxDegrees;
    }
    #endregion
}
