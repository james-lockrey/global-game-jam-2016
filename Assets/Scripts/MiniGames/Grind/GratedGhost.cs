﻿using UnityEngine;
using System.Collections;

public class GratedGhost : MonoBehaviour {
	public Turner myTurner;
	public Sprite smallGrate;
	public Sprite medGrate;
	public Sprite largeGrate;
	private SpriteRenderer myRenderer;

	void Start(){
		myRenderer = GetComponent<SpriteRenderer> ();
	}

	// Update is called once per frame
	void Update () {
		if (myTurner.maxDegrees < 1600 && myTurner.maxDegrees > 1400)
			myRenderer.sprite = smallGrate;
		if (myTurner.maxDegrees > 700 && myTurner.maxDegrees < 1100)
			myRenderer.sprite = medGrate;
		if (myTurner.maxDegrees < 400)
			myRenderer.sprite = largeGrate;
	}
}
