﻿using UnityEngine;
using System.Collections;

public class Turner : MonoBehaviour {

    bool dragging;
    new Camera camera;
    float lastAngle;

    public float grindMultiplier = 0.02f;
    public Collider2D grinder;
    public float maxDegrees = 360 * 5;
    public GrindDraggable draggable;

    void Awake() {
        camera = Camera.main;
    }

    void Update() {
        if (maxDegrees <= 0) {
            dragging = false;
        }

        if (dragging && !draggable.dragging && draggable.triggered) {
            Vector2 pos = Input.mousePosition;
            Vector2 circlePos = camera.WorldToScreenPoint(transform.parent.position);
            Debug.DrawLine(transform.parent.position, transform.parent.position + new Vector3(pos.x - circlePos.x, pos.y - circlePos.y, 0), Color.blue);

            bool isFlipped = (circlePos.x - pos.x) < 0;
            float newAngle = (isFlipped ? 0 : 180) + Mathf.Atan((circlePos.y - pos.y) / (circlePos.x - pos.x)) * Mathf.Rad2Deg;
            transform.parent.rotation = Quaternion.Euler(0, 0, newAngle);

            float angleDiff = (lastAngle - newAngle);
            // Apply grind
            if (angleDiff > 0 && maxDegrees > 0) {
                maxDegrees -= angleDiff;
                grinder.transform.position -= new Vector3(0, angleDiff * grindMultiplier, 0);
            }

            lastAngle = newAngle;
        }
    }

    void OnMouseDown() {
        dragging = true;
    }

    void OnMouseUp() {
        dragging = false;
    }
}
