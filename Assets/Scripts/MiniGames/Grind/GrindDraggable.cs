﻿using UnityEngine;
using System.Collections;

public class GrindDraggable : MonoBehaviour 
{

    new Rigidbody2D rigidbody;
    new Camera camera;
    
    [HideInInspector]
    public bool dragging;
    [HideInInspector]
    public bool triggered;

    void Awake() 
    {
        rigidbody = GetComponent<Rigidbody2D>();
        camera = Camera.main;
    }

    void Start() 
    {

    }

    void Update() 
    {
        if(dragging) 
        {
            Vector3 pos = camera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0));
            transform.position = new Vector3(pos.x, pos.y, transform.position.z);
        }
	}

    void OnMouseDown() 
    {
        if(triggered) 
        {
            return;
        }
        rigidbody.isKinematic = true;
        dragging = true;
    }

    void OnMouseUp() 
    {
        rigidbody.isKinematic = false;
        dragging = false;
    }

    void OnTriggerEnter2D() {
        triggered = true;
        dragging = false;
    }
    void OnTriggerExit2D() {
        triggered = false;
    }
}
