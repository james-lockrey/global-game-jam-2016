﻿using UnityEngine;
using System.Collections;
using System;

public class ShreadMiniGame : MiniGame
{
    [Space(10)]
    public SpriteRenderer sprite;
    public Sprite[] sprites;
    public Collider collider;
    public int targetSpriteId = 6;

    public float deltaPerLevel = 30;

    // Visible for debugging
    public float currentDelta;
    
    Vector3 lastPos;
    private int spriteId;

    #region MonoBehaviour Lifecycle
    protected override void Awake()
    {
        base.Awake();
    }

    protected override void Start()
    {
		base.Start ();

        lastPos = collider.transform.position;
    }

    protected override void Update()
    {
        base.Update();

        foreach (Touch touch in Input.touches)
        {
            if (touch.phase == TouchPhase.Moved)
            {
                RaycastHit hit;
                if (Physics.Raycast(Camera.main.ScreenPointToRay(new Vector3(touch.position.x, touch.position.y, 0)), out hit))
                {
                    if (hit.collider == collider)
                    {
                        currentDelta += (hit.point - lastPos).magnitude;
                        Debug.DrawLine(lastPos, hit.point, Color.blue, 1.0f);
                        lastPos = hit.point;

                        PerformedFirstAction = true;
                    }
                }
            }
        }

        spriteId = Mathf.Min(((int)(currentDelta / deltaPerLevel)), sprites.Length - 1);
        if (sprite.sprite != sprites[spriteId])
        {
            sprite.sprite = sprites[spriteId];
        }
    }
    #endregion

    #region Inherited Functions
    protected override float CalculateScore()
    {
        return 1.0f - (float)Mathf.Abs(spriteId - targetSpriteId) / sprites.Length;
    }
    #endregion
}
