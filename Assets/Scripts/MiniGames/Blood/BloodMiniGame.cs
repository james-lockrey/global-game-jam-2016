﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BloodMiniGame : MiniGame
{

    public Image emptyTube;
    public Image filledTube;
    public Image bloodBag;
    public Image bloodVial;

    public float pinchMultiplier = 0.2f;

    public Collider2D tubeDragger;
    public Transform tubeDragMin;
    public Transform tubeDragMax;

    TKPinchRecognizer pinchRecogniser;
    float bloodAmount;
    bool pinchable;
    bool dragging;
    float dragAmount;

    #region MonoBehaviour Lifecycle
    protected override void Start()
    {
        base.Start();

        pinchRecogniser = new TKPinchRecognizer();
        TouchKit.addGestureRecognizer(pinchRecogniser);
        pinchRecogniser.gestureRecognizedEvent += OnGesture;
    }

    void OnDisable()
    {
        TouchKit.removeGestureRecognizer(pinchRecogniser);
    }

    protected override void Update()
    {
        base.Update();

        if (dragging || Input.GetMouseButton(0))
        {
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
            if (hit != null && hit.collider == tubeDragger)
            {
                dragging = true;
            }

            if (dragging)
            {
                float x = Mathf.Clamp(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, tubeDragMin.position.x, tubeDragMax.position.x);
                tubeDragger.transform.position = new Vector3(x, tubeDragger.transform.position.y, tubeDragger.transform.position.z);
                dragAmount = (x - tubeDragMin.position.x) / (tubeDragMax.position.x - tubeDragMin.position.x);
                emptyTube.fillAmount = dragAmount;
                pinchable = (emptyTube.fillAmount > 0.9f);
            }
        }
        if (!Input.GetMouseButton(0))
        {
            dragging = false;
        }

        filledTube.fillAmount = Mathf.Min(dragAmount, bloodAmount * 10.0f);
        bloodBag.fillAmount = 1 - bloodAmount;
        bloodVial.fillAmount = (bloodAmount - (0.1f)) / 0.9f;

    }
    #endregion

    #region Events
    void OnGesture(TKPinchRecognizer gesture)
    {
        if (!pinchable)
        {
            return;
        }

        bloodAmount -= gesture.deltaScale * pinchMultiplier;
        bloodAmount = Mathf.Clamp01(bloodAmount);
    }
    #endregion

    #region Inherited Functions
    protected override float CalculateScore()
    {
        return bloodAmount;
    }
    #endregion
}
