﻿using UnityEngine;
using System.Collections;
using System;

public class HornScaleTouch : MiniGame {
	private Touch t;
	private Vector3 touchPoint;
	private Vector2 touchPoint2D;
	//Layer that the horns are on
	public int hornLayer;
	//The radius of the horns
	private int hornTapRadius = 5;
	private GameObject touchedHorn;
	//Touching a horn
	private bool touchingHorn;

	//The tap radius for the scales
	private int scaleTapRadius = 5;
	public int scaleLayer;
	private GameObject touchedScale;
	private bool touchingScale;
	//Amount of horns/scales to pull before moving onto next stage
	public int minAmount;

    private HornPull[] horns;

	#region MonoBehaviour Lifecycle
	protected override void Start ()
	{
		base.Start ();

		touchingHorn = false;
		touchingScale = false;

        horns = gameObject.GetComponentsInChildren<HornPull>();
	}

	protected override void Update ()
	{
		base.Update ();

		if (Input.touchCount > 0) {
			t = Input.touches [0];
		}

		touchPoint = Camera.main.ScreenToWorldPoint (new Vector3 (t.position.x, t.position.y, 0));
		touchPoint2D = new Vector2 (touchPoint.x, touchPoint.y);

		Collider2D hornCollider = Physics2D.OverlapCircle (touchPoint2D, hornTapRadius, 1 << hornLayer);
		if (hornCollider != null) {
			touchedHorn = hornCollider.transform.gameObject;
			if (t.phase == TouchPhase.Began) {
				touchingHorn = true;
				touchedHorn.SendMessage ("HornTouched");
                PerformedFirstAction = true;
			}
			if (t.phase == TouchPhase.Ended) {
				if (touchingHorn) {
					//Call the horn touch end function and pass in the change in position
					touchedHorn.SendMessage ("HornTouchEnded", t.deltaPosition);
					touchingHorn = false;
					PerformedFirstAction = true;
				}
			}
		}

        //Collider2D scaleCollider = Physics2D.OverlapCircle (touchPoint2D, scaleTapRadius, 1 << scaleLayer);
        //if (scaleCollider != null) {
        //    touchedScale = scaleCollider.transform.gameObject;
        //    if (t.phase == TouchPhase.Began) {
        //        touchingScale = true;
        //        touchedScale.SendMessage ("ScaleTouched");
        //    }
        //    if (t.phase == TouchPhase.Ended) {
        //        if (touchingScale) {
        //            //Call the horn touch end function and pass in the change in position
        //            touchedScale.SendMessage ("ScaleTouchEnded", t.deltaPosition);
        //            touchingScale = false;
        //        }
        //    }
        //}

        Vector2 mousePosWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        if(Input.GetMouseButtonDown(0)) 
        {
            Collider2D[] scales = Physics2D.OverlapCircleAll(mousePosWorld, 0.1f, 1 << scaleLayer);
            foreach (Collider2D scale in scales) 
            {
                Destroy(scale.gameObject);
                PerformedFirstAction = true;
            }
        }
	}
	#endregion

	#region Inherited Functions
    protected override float CalculateScore() {
        int pulled = 0;
        foreach(HornPull horn in horns) 
        {
            pulled += (horn.Pulled ? 1 : 0);
        }

        return pulled / horns.Length;
    }
	#endregion
}
