﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

using Random = UnityEngine.Random;

public class EggSpawner : MiniGame
{
    [Space(10)]
    public GameObject Egg1;
    public GameObject Egg2;
    public GameObject Egg3;

    public Transform Min;
    public Transform Max;

    public int riceCount = 30;
    public float sensitvity = 0.3f;
    public float maxMag = 0.8f;

    public float targetDistanceFromSpawn;
    public bool isSpreadingOut;

    private GameObject[] allTheRice;

    private bool mouseButtonDown = false;

    private Vector3 mouseDragDirection;
    private Vector3 mouseOrigPos;
    private float mouseDragMag = 0f;

    private Ray ray;
    private Ray rayLeft;
    private Ray rayRight;

    private Vector3 spawnCenter;
    private float startingAvgDistance;
    public float currentAvgDistance;

	[Header("Sounds")]
	public SoundModule squelchSounds;

    #region MonoBehaviour Lifecycle
    protected override void Awake()
    {
		base.Awake();

        if (Min == null || Max == null || Egg1 == null || Egg2 == null || Egg3 == null)
        {
            Debug.LogError("Some components are not propperly assigned", this);
        }

        allTheRice = new GameObject[riceCount];

        spawnCenter = (Min.transform.position + Max.transform.position) / 2;

        float avgDistance = 0;
        for (int i = 0; i < allTheRice.Length; i++)
        {
            int ran = Random.Range(0, 3);
            if (ran == 0)
            {
                allTheRice[i] = GameObject.Instantiate(Egg1);
            }
            else if (ran == 1)
            {
                allTheRice[i] = GameObject.Instantiate(Egg2);
            }
            else {
                allTheRice[i] = GameObject.Instantiate(Egg3);
            }
            Quaternion rot = Random.rotation;
            rot.x = 0f; rot.y = 0f;
            allTheRice[i].transform.rotation = rot;
            allTheRice[i].transform.parent = transform;


            float x, y = 0f;
            x = Random.Range(Mathf.Min(Min.position.x, Max.position.x), Mathf.Max(Min.position.x, Max.position.x));
            y = Random.Range(Mathf.Min(Min.position.y, Max.position.y), Mathf.Max(Min.position.y, Max.position.y));
            Vector3 pos = Vector3.zero;
            pos.x = x; pos.y = y;

            allTheRice[i].transform.position = pos;
            avgDistance += (pos - spawnCenter).magnitude;
        }

        avgDistance /= allTheRice.Length;
        mouseDragDirection = Vector3.zero;
        mouseOrigPos = Vector3.zero;
    }
    
    protected override void Update()
    {
        base.Update();

        MouseInputHandler();
        MoveRice();
    }
    #endregion

    #region Inherited Functions
    protected override float CalculateScore()
    {
        currentAvgDistance = 0;
        for (int i = 0; i < allTheRice.Length; i++) 
        {
            currentAvgDistance += (allTheRice[i].transform.position - spawnCenter).magnitude;
        }
        currentAvgDistance /= allTheRice.Length;

        if(isSpreadingOut) 
        {
            return (currentAvgDistance - startingAvgDistance) / (targetDistanceFromSpawn - startingAvgDistance);
        } 
        else
        {
            return 1.0f - (currentAvgDistance - targetDistanceFromSpawn) / (startingAvgDistance - targetDistanceFromSpawn);
        }
    }
    #endregion

    void MouseInputHandler()
    {
        // Check when we have pressed the mouse Button / Tapped the screen
        if (Input.GetMouseButtonDown(0))
        {
            mouseOrigPos = Input.mousePosition;

            ////We have only just pressed the button so return so that we can get the directon that the mouse has moved
            ////if at al on the next frame
            //return;
        }

        if (Input.GetMouseButton(0))
        {
            mouseButtonDown = true;
            mouseDragMag = Mathf.Min((mouseOrigPos - Input.mousePosition).magnitude * sensitvity, 0.6f);
            mouseDragDirection = (Input.mousePosition - mouseOrigPos).normalized;
            mouseOrigPos = Input.mousePosition;
        }

        if (Input.GetMouseButtonUp(0))
        {
            mouseButtonDown = false;
        }
    }

    void MoveRice()
    {
        if (mouseButtonDown)
        {
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            Transform mouse = transform.GetChild(0).transform;
            mouse.position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mouse.LookAt(mouse.position + ray.direction.normalized);
            mouse.Rotate(Vector3.up, -3f);
            rayLeft = new Ray(mouse.position, mouse.forward);
            mouse.Rotate(Vector3.up, 6f);
            rayRight = new Ray(mouse.position, mouse.forward);
            mouse.Rotate(Vector3.up, -3f);

            RaycastHit[] hits = Physics.RaycastAll(ray);

            if (hits.Length > 0)
            {
                //We have hit something   
                for (int i = 0; (i < hits.Length) && (i < 4); i++)
                {
                    hits[i].collider.gameObject.GetComponent<Eggs>().SetVelocity(mouseDragDirection * mouseDragMag);
                }

				if(squelchSounds != null)
					SoundKit.instance.playOneShot(squelchSounds.Clip);
            }

            PerformedFirstAction = true;
        }
    }

    //void OnDrawGizmos() {
    //    Gizmos.color = Color.red;

    //    Gizmos.DrawSphere(transform.GetChild(0).transform.position, .2f);

    //    Gizmos.color = Color.blue;

    //    Gizmos.DrawLine(ray.origin, ray.origin + ray.direction * 20f);
    //    Gizmos.DrawLine(rayLeft.origin, rayLeft.origin + rayLeft.direction * 20f);
    //    Gizmos.DrawLine(rayRight.origin, rayRight.origin + rayRight.direction * 20f);
    //}
}
