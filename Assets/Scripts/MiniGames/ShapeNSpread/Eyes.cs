﻿using UnityEngine;
using System.Collections;

public class Eyes : MonoBehaviour {

    public float sleepVal = 0.05f;
    public float slowTime = 0.5f;
    public AnimationCurve slowCurve;

    private Vector3 velocity;

    private float timer = 0f;

	// Use this for initialization
	void Start () {
        velocity = Vector3.zero;
	}

    void Update() {
        timer += Time.deltaTime;
    }

	// Update is called once per frame
	void FixedUpdate () {
        if (velocity.magnitude > sleepVal) {
            transform.position += velocity * slowCurve.Evaluate(timer / slowTime);
        } else {
            velocity = Vector3.zero;
        }
	}

    public void SetVelocity(Vector3 vel) {
        velocity = vel;
        timer = 0f;
    }
}
