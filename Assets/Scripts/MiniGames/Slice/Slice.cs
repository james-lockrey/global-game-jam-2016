﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Slice : MonoBehaviour {

    public float force = 2;

	void Start () {
    }
	
	void Update () {
	
	}

    void OnCollisionEnter2D(Collision2D collision) {
        Debug.Log("asd" + collision.contacts.Length);
        
    }

    public void PerformSlice(Vector3 startSlice, Vector3 endSlice) {
        // We are in 2D space
        startSlice.z = 0;
        endSlice.z = 0;

        // Move so start of slice is at origin to avoid em bugz
        //Vector3 offset = startSlice;
        //imageCollider.transform.parent.position -= startSlice;
        //startSlice = Vector3.zero;
        //endSlice -= offset;

        // Find the two points it intersects in the image
        Vector3 bigA = startSlice + (endSlice - startSlice) * 100;
        Vector3 bigB = endSlice - (endSlice - startSlice) * 100;
        Debug.DrawLine(bigA, bigB, Color.red, 5);

        float distance;
        BoxCollider2D collider = GetComponentInChildren<BoxCollider2D>();
        collider.bounds.IntersectRay(new Ray(bigA, bigB), out distance);
        Vector3 intA = bigA + (bigB - bigA).normalized * distance;
        Debug.DrawLine(bigA, intA, Color.green, 5);
        collider.bounds.IntersectRay(new Ray(bigB, bigA), out distance);
        Vector3 intB = bigB - (bigB - bigA).normalized * distance;
        Debug.DrawLine(bigB, intB, Color.green, 5);

        // Find the center point of that
        Vector3 intCenter = (intA + intB) / 2;
        Vector3 intNorm = (intB - intA).normalized;
        Vector3 intPerp = Quaternion.Euler(0, 0, 90) * intNorm;
        Debug.DrawRay(intCenter, intPerp, Color.blue, 5);

        // Create a new mask for this cut
        RectTransform newMask = new GameObject(this.name + " dupe").AddComponent<RectTransform>();
        newMask.gameObject.AddComponent<Image>();
        newMask.gameObject.AddComponent<Mask>().showMaskGraphic = false;
        newMask.sizeDelta = (transform as RectTransform).sizeDelta;
        newMask.gameObject.AddComponent<Slice>();
        newMask.gameObject.AddComponent<SliceMove>();
        newMask.GetComponent<Slice>().force = force; 
        newMask.GetComponent<SliceMove>().slowdown = GetComponent<SliceMove>().slowdown;    

        newMask.GetComponentInChildren<Slice>().enabled = false;
        while (newMask.childCount > 0) {
            Transform child = newMask.GetChild(0);
            child.parent = null;
            Destroy(child);
        }
        newMask.parent = transform.parent;
        transform.parent = newMask;

        // Rotate parent mask to match angle, position it on the edge
        Vector3 origPos = newMask.GetChild(0).position;
        newMask.rotation = Quaternion.Euler(0, 0, Mathf.Atan((intB.y - intA.y) / (intB.x - intA.x)) * Mathf.Rad2Deg);
        newMask.position = intCenter + intPerp * (newMask as RectTransform).rect.height / 2;

        newMask.GetChild(0).rotation = Quaternion.Euler(Vector3.zero);
        newMask.GetChild(0).position = origPos;
        
        // Duplicate and do the same 
        RectTransform dupe = GameObject.Instantiate(newMask) as RectTransform;
        dupe.transform.position = intCenter - intPerp * (newMask as RectTransform).rect.height / 2;
        dupe.transform.GetChild(0).position = origPos;
        dupe.transform.parent = newMask.parent;

        // Move back
        //imageCollider.transform.parent.position += offset;
        //dupe.transform.position += offset;

        // Move em
        SliceMove move;
        if (move = newMask.GetComponentInParent<SliceMove>()) {
            move.vel = intPerp * force;
            dupe.GetComponentInParent<Slice>().force /= 4;
        }
        if (move = dupe.GetComponent<SliceMove>()) {
            move.vel = -intPerp * force;
            dupe.GetComponent<Slice>().force /= 4;
        }
    }
}
