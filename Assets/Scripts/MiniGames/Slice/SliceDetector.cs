﻿using UnityEngine;
using System.Collections;
using Prime31;

public class SliceDetector : MonoBehaviour {

    TKSwipeRecognizer swipeRecognition;
    new Camera camera;

	void Start () {
        swipeRecognition = new TKSwipeRecognizer();
        swipeRecognition.gestureRecognizedEvent += OnGesture;
        TouchKit.addGestureRecognizer(swipeRecognition);
        camera = Camera.main;
	}

    void OnGesture(TKSwipeRecognizer swipe) {
        Vector3 start;
        Vector3 end;

        // Calculate hit in world space
        RaycastHit hit;
        Physics.Raycast(camera.ScreenPointToRay(swipe.startPoint), out hit);
        start = hit.point;
        Physics.Raycast(camera.ScreenPointToRay(swipe.endPoint), out hit);
        end = hit.point;

        //end = new Vector3(-2, -2, 0);
        //start = Vector3.zero;
        Debug.DrawLine(start, end, Color.yellow, 5);

        // We are in 2D
        start.z = 0;
        end.z = 0;

        // Send to slicers
        foreach (Slice slice in GetComponentsInChildren<Slice>()) {
            slice.PerformSlice(start, end);
        }
    }

    void OnDestroy() {
        TouchKit.removeGestureRecognizer(swipeRecognition);
    }
	
	// Update is called once per frame
	void Update () {
	    
	}
}
