﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SliceMiniGame : MiniGame 
{
	List<SpriteSlicer2DSliceInfo> slicedSpriteInfo = new List<SpriteSlicer2DSliceInfo>();
	TrailRenderer trailRenderer;

	struct MousePosition
	{
		public Vector3 pos;
		public float time;
	}

    // Target/objective stuff
    [Header("Target Objective Properties")]
    public Transform targetSliceTransform;
    public float distanceMultiplier = 20.0f;

    [Header("Angled Objective Properties")]
    public int targetSliceCount = 0;
    public float targetSliceAngle = Mathf.PI / 4 * Mathf.Rad2Deg;
    public float targetSliceAccuracy = Mathf.PI / 4 * Mathf.Rad2Deg;

    // Internal target/objective stuff
    Vector3 targetSlicePos;
    float closestToSlice = float.MaxValue - 1;

    // Make public just for debugging
    public float avgSliceAngle = 0;

	List<MousePosition> mousePositions = new List<MousePosition>();
	float mouseTimer = 0.0f;
	float mouseRecordInt = 0.05f;
	int maxMousePositions = 5;
    int sliceCount;

	[Header("Sounds")]
	public SoundModule sliceSounds;

    #region MonoBehaviour Lifecycle
    protected override void Start ()
	{
		base.Start ();
		trailRenderer = GetComponentInChildren<TrailRenderer>();
        if (targetSliceTransform) 
        {
            targetSlicePos = targetSliceTransform.position;
        }
	}

	protected override void Update () 
	{
        base.Update();

		// Left mouse button - hold and swipe to cut objects
		if(Input.GetMouseButton(0))
		{
			bool mousePositionAdded = false;
			mouseTimer -= Time.deltaTime;

			// Record the world position of the mouse every x seconds
			if(mouseTimer <= 0.0f)
			{
				MousePosition newMousePosition = new MousePosition();
				newMousePosition.pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
				newMousePosition.time = Time.time;

				mousePositions.Add(newMousePosition);
				mouseTimer = mouseRecordInt;
				mousePositionAdded = true;

				// Remove the first recorded point if we've recorded too many
				if(mousePositions.Count > maxMousePositions)
				{
					mousePositions.RemoveAt(0);
				}
			}

			// Forget any positions that are too old to care about
			if(mousePositions.Count > 0 && (Time.time - mousePositions[0].time) > mouseRecordInt * maxMousePositions)
			{
				mousePositions.RemoveAt(0);
			}

			// Go through all our recorded positions and slice any sprites that intersect them
			if(mousePositionAdded)
			{
				for(int loop = 0; loop < mousePositions.Count - 1; loop++)
				{
                    Vector3 a = mousePositions[loop].pos;
                    Vector3 b = mousePositions[mousePositions.Count - 1].pos;
                    // Make our slices a lil bigger :)
                    a = a - (b - a);
                    b = b + (b - a);

					SpriteSlicer2D.SliceAllSprites(a, b, true, ref slicedSpriteInfo);

                    // Calculate dist
                    if (targetSliceTransform) 
                    {
                        // Faaaat formula
                        float top = Mathf.Abs((b.y - a.y) * targetSlicePos.x - (b.x - a.x) * targetSlicePos.y + b.x * a.y - b.y * a.x);
                        float bot = Mathf.Sqrt(Mathf.Pow(b.y - a.y, 2) + Mathf.Pow(b.x - a.x, 2));
                        closestToSlice = Mathf.Min(closestToSlice, top / bot);
                    }
                    
                    float alpha = 0.9f;
                    float angle = Mathf.Abs(Mathf.Atan((b.y - a.y) / (b.x - a.x)));
                    if (!float.IsNaN(angle) && !float.IsInfinity(angle)) 
                    {
                        avgSliceAngle = (1 - alpha) * avgSliceAngle + alpha * angle;
                    }

					if(slicedSpriteInfo.Count > 0)
					{
						// Add some force in the direction of the swipe so that stuff topples over rather than just being
						// sliced but remaining stationary
						for(int spriteIndex = 0; spriteIndex < slicedSpriteInfo.Count; spriteIndex++)
						{
							for(int childSprite = 0; childSprite < slicedSpriteInfo[spriteIndex].ChildObjects.Count; childSprite++)
							{
								Vector2 sliceDirection = mousePositions[mousePositions.Count - 1].pos - mousePositions[loop].pos;
								sliceDirection.Normalize();
                                Vector3 perpSliceDirection = Quaternion.Euler(0, 0, (childSprite == 0 ? 90 : 270)) * new Vector3(sliceDirection.x, sliceDirection.y, 0);
								
                                slicedSpriteInfo[spriteIndex].ChildObjects[childSprite].GetComponent<Rigidbody2D>().AddForce(new Vector2(perpSliceDirection.x, perpSliceDirection.y) * 100.0f);
                                PerformedFirstAction = true;
							}

                            sliceCount += slicedSpriteInfo[spriteIndex].ChildObjects.Count;

							// Play slice sound
							if(sliceSounds != null)
								SoundKit.instance.playOneShot(sliceSounds.Clip);
						}

						mousePositions.Clear();
						break;
					}
				}
			}

			if(trailRenderer)
			{
				Vector3 trailPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
				trailPosition.z = -9.0f;
				trailRenderer.transform.position = trailPosition;
			}
		}
		else
		{
			mousePositions.Clear();
		}

		// Sliced sprites sharing the same layer as standard Unity sprites could increase the draw call count as
		// the engine will have to keep swapping between rendering SlicedSprites and Unity Sprites.To avoid this, 
		// move the newly sliced sprites either forward or back along the z-axis after they are created
		for(int spriteIndex = 0; spriteIndex < slicedSpriteInfo.Count; spriteIndex++)
		{
			for(int childSprite = 0; childSprite < slicedSpriteInfo[spriteIndex].ChildObjects.Count; childSprite++)
			{
				Vector3 spritePosition = slicedSpriteInfo[spriteIndex].ChildObjects[childSprite].transform.position;
				spritePosition.z = -1.0f;
				slicedSpriteInfo[spriteIndex].ChildObjects[childSprite].transform.position = spritePosition;
			}
		}

		slicedSpriteInfo.Clear();
	}
    #endregion

    #region Inherited Functions
    protected override float CalculateScore()
    {
        float score = 0f;

        if (targetSliceTransform)
        {
            score = Mathf.Clamp01(1.0f - closestToSlice / distanceMultiplier);
        }

        if(targetSliceCount > 0)
        {
            float angleDiff = Mathf.Abs(avgSliceAngle * Mathf.Rad2Deg - targetSliceAngle);
            score = Mathf.Clamp01(1.0f - angleDiff / targetSliceAccuracy);
            // Penalise for missing count
            int diff = Mathf.Abs((sliceCount / 2) - targetSliceCount);
            if (diff > 0)
            {
                score /= diff;
            }
        }

        return score;
    }
    #endregion
}
