﻿using UnityEngine;
using System.Collections;

// Why didnt I write this before the jam omg lel 
public class SliceMove : MonoBehaviour {

    public Vector3 vel;
    public float slowdown = 8;

    new Transform transform;

    void Start() {
        transform = GetComponent<Transform>();
    }

    void Update() {
        transform.position += vel * Time.deltaTime;
        vel = Vector3.Lerp(vel, Vector3.zero, Time.deltaTime * slowdown);
    }
}
