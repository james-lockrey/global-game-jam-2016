﻿using UnityEngine;
using System.Collections;

public class SwipeInput : MonoBehaviour {
	private Touch t;
	private Vector3 touchPoint;
	private Vector2 touchPoint2D;
	//Number of touches currently used
	private int touches;
	//The sushi to slice
	public Spells mySpell;
	//Is currently touching
	private bool touching;
	//Checkpoint count
	private int checkpointCount;

	void Start(){
		checkpointCount = 0;
	}

	// Update is called once per frame
	void Update () {
		if (Input.touchCount > 0) {
			t = Input.touches [0];
		}

		touchPoint = Camera.main.ScreenToWorldPoint (new Vector3 (t.position.x, t.position.y, 0));
		touchPoint2D = new Vector2 (touchPoint.x, touchPoint.y);

        if(mySpell == null) {
			//Debug.Log ("no spell found");
			GameObject[] objs = GameObject.FindGameObjectsWithTag ("Horizontal");
			//Debug.Log (objs.Length);
			for (int i = 0; i < objs.Length; i++) {
				if (objs [i].activeSelf) {
					mySpell = objs [i].GetComponent<Spells> ();
					break;
				}
			}
            return;
        }

		//Debug.Log (touchPoint);
		if (mySpell.numOfCheckPoints > 0) {
			Debug.Log ("num of checkpoints " + mySpell.numOfCheckPoints);
			for (int i = 0; i < mySpell.numOfCheckPoints; i++) {
				Debug.Log ("Checking checkpoint " + i + " "+mySpell.myCheckPoints[i].check);
				//Calculate the max & min thresholds
				mySpell.myCheckPoints [i].maxX = Mathf.Max (mySpell.myCheckPoints [i].positionA.x, mySpell.myCheckPoints [i].positionB.x);
				mySpell.myCheckPoints [i].maxY = Mathf.Max (mySpell.myCheckPoints [i].positionA.y, mySpell.myCheckPoints [i].positionB.y);
				mySpell.myCheckPoints [i].minX = Mathf.Min (mySpell.myCheckPoints [i].positionA.x, mySpell.myCheckPoints [i].positionB.x);
				mySpell.myCheckPoints [i].minY = Mathf.Min (mySpell.myCheckPoints [i].positionA.y, mySpell.myCheckPoints [i].positionB.y);

				//Draws boxes to indicate where the checkpoints lie
				Debug.DrawLine (new Vector3 (mySpell.myCheckPoints [i].maxX, mySpell.myCheckPoints [i].maxY), new Vector3 (mySpell.myCheckPoints [i].maxX, mySpell.myCheckPoints [i].minY), Color.black, 1000);
				Debug.DrawLine (new Vector3 (mySpell.myCheckPoints [i].minX, mySpell.myCheckPoints [i].maxY), new Vector3 (mySpell.myCheckPoints [i].minX, mySpell.myCheckPoints [i].minY), Color.red, 1000);
				Debug.DrawLine (new Vector3 (mySpell.myCheckPoints [i].minX, mySpell.myCheckPoints [i].maxY), new Vector3 (mySpell.myCheckPoints [i].maxX, mySpell.myCheckPoints [i].maxY), Color.blue, 1000);
				Debug.DrawLine (new Vector3 (mySpell.myCheckPoints [i].minX, mySpell.myCheckPoints [i].minY), new Vector3 (mySpell.myCheckPoints [i].maxX, mySpell.myCheckPoints [i].minY), Color.green, 1000);

				//If it is within the checkpoint's
				if (touchPoint2D.x > mySpell.myCheckPoints [i].minX && touchPoint2D.x < mySpell.myCheckPoints [i].maxX &&
				      touchPoint2D.y > mySpell.myCheckPoints [i].minY && touchPoint2D.y < mySpell.myCheckPoints [i].maxY) {
					//Check the checkpoint order
					if (i <= checkpointCount) {
						if (mySpell.myCheckPoints [i].check == false) {
							//Set the point as checked
							mySpell.myCheckPoints [i] = new Spells.checkPoint (mySpell.myCheckPoints [i].positionA, mySpell.myCheckPoints [i].positionB, 
								mySpell.myCheckPoints [i].center, mySpell.myCheckPoints [i].minX, mySpell.myCheckPoints [i].minY,
								mySpell.myCheckPoints [i].maxX, mySpell.myCheckPoints [i].maxY, true);
							checkpointCount++;
						}
					} else {
						//Spell failed
						Debug.Log ("Spell failed. Out of order.");
						break;
					}
				} 
			}
		}

		if (t.phase == TouchPhase.Began) {
			touching = true;
		}

		if (t.phase == TouchPhase.Ended) {
			if (touching) {
				touches++;
				if (touches > mySpell.numOfTouches) {
					//Too many touches
					Debug.Log ("too many touches");
					//Spells failed
					Debug.Log ("Spells failed");
				} else if (touches == mySpell.numOfTouches) {
					//Reset touches
					touches = 0; 
					mySpell = GameObject.FindGameObjectWithTag ("Horizontal").GetComponent<Spells>();
					mySpell.SendMessage ("CheckGesture");
				}
				touching = false;
			}
		}
	}
}
