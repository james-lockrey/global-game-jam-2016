﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour
{
    private static GameManager instance = null;
    public static GameManager Instance { get { return instance; } }

    public delegate void GameStartDelegate();
    public static event GameStartDelegate OnGameStart = delegate { };
    public delegate void GameExitDelegate();
    public static event GameExitDelegate OnGameExit = delegate { };

    #region Properties
    public int TotalEarnings
    {
        get { return Core.Instance.SaveManager.TotalEarnings; }
        set { Core.Instance.SaveManager.TotalEarnings = value; }
    }
    #endregion

    #region MonoBehaviour Lifecycle
    void Awake()
    {
        if(instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        else
        {
            instance = this;
        }
    }

    void Start()
    {
        DontDestroyOnLoad(gameObject);
    }
    #endregion

    #region Events
    // Get here from starting a sequence after a list of dishes has been made to fulfill an order
    private void OnSequenceStart(List<MiniGame> chosenSequence)
    {

    }

    // Get here after finishing a sequence. Exiting ends up at game exit instead
    private void OnSequenceEnd()
    {

    }
    #endregion

    public void StartGame()
    {
        OnGameStart();
    }

    public void ExitGame()
    {
        OnGameExit();
    }
}
