﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Prime31;

public class SequenceManager : MonoBehaviour
{
    private static SequenceManager instance = null;
    public static SequenceManager Instance { get { return instance; } }

    private List<MiniGame> currentSequence = new List<MiniGame>();
    private int sequencePosition = 0;

	private float totalScore;

    [Space(10)]
    public SequenceProgress sequenceProgress;

    [Space(10)]
    public GameObject sequenceContainer;

    [Space(10)]
    public Text commandText;
    public Text timeRemainingText;
    public Button nextButton;

	[Header("Sounds")]
	public SoundModule swishSounds;

    public delegate void MiniGameCompleteDelegate(MiniGame completed);
    public static event MiniGameCompleteDelegate OnMiniGameComplete = delegate { };
    public delegate void NewMiniGameReadyDelegate(MiniGame current);
    public static event NewMiniGameReadyDelegate OnNewMiniGameReady = delegate { };
    public delegate void SequenceCompleteDelegate(float accuracyPercentage);
    public static event SequenceCompleteDelegate OnSequenceComplete = delegate { };
    
    #region Properties
    public int SequencePosition { get { return sequencePosition; } }
    public MiniGame CurrentMiniGame { get { return currentSequence[sequencePosition]; } }
    #endregion

    #region MonoBehaviour Lifecycle
    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(gameObject);
            return;
        }
        else
        {
            instance = this;
        }

        commandText.gameObject.SetActive(false);

        BarManager.OnSequenceStarted += OnNewSequence;
    }

    void Update()
    {
        timeRemainingText.text = CurrentMiniGame.TimeRemaining.ToString("0.0");
        nextButton.interactable = CurrentMiniGame.PerformedFirstAction;
    }
    #endregion

    #region Events
    private void OnNewSequence(List<MiniGame> sequence)
    {
        gameObject.SetActive(true);

        currentSequence.Clear();
        sequencePosition = 0;

		totalScore = 0f;

        for(int i = 0; i < sequence.Count; i++)
        {
            // Spawn and position each minigame next to each other
            GameObject go = (GameObject)Instantiate(sequence[i].gameObject, new Vector3(16f * UIManager.Instance.RelativeAspect * i, 0, 0), Quaternion.identity);
			go.transform.SetParent(sequenceContainer.transform);
            go.SetActive(i == 0);

            // Get minigame component and disable it by default
            MiniGame mg = go.GetComponent<MiniGame>();
            mg.enabled = i == 0;
            currentSequence.Add(mg);
        }

		commandText.text = CurrentMiniGame.description;
		commandText.gameObject.SetActive(true);

        CameraKit2D.instance.targetCollider = CurrentMiniGame.Collider2D;
    }
    #endregion

    public void FireMiniGameComplete(MiniGame miniGame, float time, float normalisedRating)
    {
        if(miniGame != CurrentMiniGame) 
        {
            return;
        }
		Debug.Log(string.Format("Minigame complete. Game: {0} Time: {1} Rating: {2}", miniGame.name, time, Mathf.RoundToInt(normalisedRating * 100)));

		totalScore += normalisedRating;

        // Just launch the next minigame for now
        NextMiniGame();
    }

    public void NextMiniGame()
    {
        if (sequencePosition < currentSequence.Count - 1)
        {
            sequencePosition++;
            // Lerp the camera to the center of the new page
            CameraKit2D.instance.targetCollider = CurrentMiniGame.Collider2D;
            CurrentMiniGame.gameObject.SetActive(true);

			commandText.gameObject.SetActive(false);
            OnMiniGameComplete(currentSequence[sequencePosition - 1]);
            StartCoroutine(CR_LerpToNewMiniGame());
        }
        else
        {
            StartCoroutine(CR_LerpToEndMiniGame());
        }
    }

    private void CompleteSequence()
    {
        OnSequenceComplete(totalScore / (float)currentSequence.Count);

        CameraKit2D.instance.transform.position = new Vector3(0, 0, -10);
        CameraKit2D.instance.targetCollider = sequenceContainer.GetComponent<Collider2D>();

        for(int i = currentSequence.Count - 1; i >= 0; i--)
            Destroy(currentSequence[i].gameObject);

        gameObject.SetActive(false);
    }

    private IEnumerator CR_LerpToNewMiniGame()
    {
		if(swishSounds != null)
			SoundKit.instance.playOneShot(swishSounds.Clip);

        yield return new WaitForSeconds(1f);

		commandText.text = CurrentMiniGame.description;
		commandText.gameObject.SetActive(true);

        OnNewMiniGameReady(CurrentMiniGame);
    }

    private IEnumerator CR_LerpToEndMiniGame() {
        yield return new WaitForSeconds(0.5f);
        CompleteSequence();
    }

    private IEnumerator CR_ShowCommand(string description)
    {
        commandText.text = description;
        commandText.gameObject.SetActive(true);
        yield return new WaitForSeconds(5f); 
        commandText.gameObject.SetActive(false);
    }
}
