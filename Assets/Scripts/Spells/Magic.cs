﻿using UnityEngine;
using System.Collections;
using System;

public class Magic : MiniGame {

    public float fadeTime = 0.25f;

    public GameObject[] spellGesturepreFabs;
    private GameObject[] spellGestures;

    private bool succedded = false;
    private bool fade = false;

    private float timer = 0f;
    private int currentGesture = 0;

    private Color col;

	// Use this for initialization
	protected override void Start () {
        base.Start();
        spellGestures = new GameObject[spellGesturepreFabs.Length]; 

	    for(int i = 0; i < spellGesturepreFabs.Length; i++) {
            spellGestures[i] = GameObject.Instantiate(spellGesturepreFabs[i]);
            spellGestures[i].transform.parent = transform;
            spellGestures[i].SetActive(false); 
        }

        col = spellGestures[currentGesture].GetComponent<SpriteRenderer>().color;
        spellGestures[currentGesture].SetActive(true);
	}

    protected override void Update() {
        //base.Update();
        if (fade) {
            timer += Time.deltaTime;

            if(timer < fadeTime) {
                col.a = (fadeTime - timer) / fadeTime;
                spellGestures[currentGesture].GetComponent<SpriteRenderer>().color = col;
            } else {
                timer = 0f;
                fade = false;
                NextStep();
            }
        }else if (spellGestures[currentGesture].GetComponent<Spells>().correctGesture) {
            MoveToNextGesture();
            //spellGestures[currentGesture].GetComponent<Spells>().correctGesture = false;
        }
        //Debug.Log(spellGestures[currentGesture].GetComponent<Spells>().correctGesture);
    }

    private void NextStep() {
        spellGestures[currentGesture].SetActive(false);
        currentGesture++;
        col = spellGestures[currentGesture].GetComponent<SpriteRenderer>().color;
        if (currentGesture != spellGestures.Length) 
            spellGestures[currentGesture].SetActive(true);
    }

    private void MoveToNextGesture() {
        fade = true;
    }

    protected override float CalculateScore() {
        //throw new NotImplementedException();
        return 0f;
    }
}
