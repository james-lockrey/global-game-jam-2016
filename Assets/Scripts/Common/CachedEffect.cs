﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(ParticleSystem))]
public class CachedEffect : MonoBehaviour 
{
	private ParticleSystem _particleSystem;
	
	#region Properties
	public bool IsPlaying
	{
		get { return _particleSystem.isPlaying; }
	}
	#endregion
	
	void Awake()
	{
		_particleSystem = GetComponent<ParticleSystem>();
	}
	
	void Start()
	{
		_particleSystem.Stop();
		gameObject.SetActive(false);
	}
	
	void OnEnable()
	{
		_particleSystem.Play(true);
	}
	
	public void Play()
	{
		if (_particleSystem.isPlaying)
		{
			_particleSystem.Stop(true);
			_particleSystem.Clear(true);
		}
		else
		{
			gameObject.SetActive(true);
		}
		
		_particleSystem.Play(true);
	}
	
	public void Play(float duration)
	{
		if (_particleSystem.isPlaying)
		{
			_particleSystem.Clear(true);
		}
		else
		{
			gameObject.SetActive(true);
		}
		
		StartCoroutine(CR_PlayEffect(duration));
	}
	
	public void Stop()
	{
		if (_particleSystem.isPlaying)
		{
			_particleSystem.Stop(true);
			_particleSystem.Clear(true);
		}
		
		gameObject.SetActive(false);
	}
	
	private IEnumerator CR_PlayEffect(float duration)
	{
		_particleSystem.Play(true);
		yield return new WaitForSeconds(duration);
		
		gameObject.SetActive(false);
	}
	
	void OnDisable()
	{
		_particleSystem.Stop(true);
		_particleSystem.Clear(true);
	}
}