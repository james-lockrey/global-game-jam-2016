﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(ParticleSystem))]
public class SetParticleSortingLayer : MonoBehaviour
{
	public string sortingLayerName;

	#region MonoBehaviour Lifecycle
	void Start()
	{
		GetComponent<ParticleSystem>().GetComponent<Renderer>().sortingLayerName = sortingLayerName;

		Destroy(this);
	}
	#endregion
}
