﻿using UnityEngine;
using System.Collections;

public class Core : MonoBehaviour
{
	private static Core instance = null;
	public static Core Instance { get { return instance; } }

    public bool Reset;
    
    [Space(10)]
	public bool hasTargetFramerate;
	public int targetFramerate;

    private SaveManager saveManager;

    #region Properties
    public SaveManager SaveManager { get { return saveManager; } }
    #endregion

    #region MonoBehaviour Lifecycle
    void Awake()
	{
		// Ensures that the Core object persists across all game scenes,
		//  and that only one instance is maintained
		if (instance != null && instance != this)
		{
			Destroy(this.gameObject);
			return;
		}
		else
		{
			instance = this;
		}

        saveManager = new SaveManager();
        saveManager.Load();
		
		DontDestroyOnLoad(this);
	}
	
	void Start()
	{
		if (hasTargetFramerate)
		{
			Application.targetFrameRate = targetFramerate;
		}
	}
	#endregion
}