﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Renderer))]
public class RenderLayerSorter : MonoBehaviour
{
	public string sortingLayerName;
	public int sortingOrder;
	public int renderQueue = -1;
	
	#region MonoBehaviour Lifecycle
	void Awake()
	{
		Renderer r = GetComponent<Renderer>();
		
		if(!string.IsNullOrEmpty(sortingLayerName))
			r.sortingLayerName = sortingLayerName;
		
		r.sortingOrder = sortingOrder;
		
		if(renderQueue > 0)
			r.sharedMaterial.renderQueue = renderQueue;
	}
	#endregion
}