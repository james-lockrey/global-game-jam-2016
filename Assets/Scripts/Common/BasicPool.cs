﻿using System.Collections.Generic;
using UnityEngine;

public class BasicPool
{
	private List<GameObject> pool;
	
	private GameObject prefab;
	private int initPoolSize;
	private int poolSize;
	private Transform parentTransform;
	private string baseName;
	
	public BasicPool(GameObject prefab, string baseName, int poolSize, Transform parentTransform)
	{
		pool = new List<GameObject>();
		
		this.prefab = prefab;
		this.poolSize = initPoolSize = poolSize;
		this.parentTransform = parentTransform;
		this.baseName = baseName;
		
		for (int i = 0; i < poolSize; i++)
		{
			GameObject item = GameObject.Instantiate(prefab, Vector3.zero, prefab.transform.localRotation) as GameObject;
			item.name = baseName + '-' + i.ToString();
			item.transform.parent = parentTransform;
			item.SetActive(false);
			pool.Add(item);
		}
	}
	
	public GameObject GetNextInactive()
	{
		for (int i = 0; i < pool.Count; i++)
		{
			if (!pool[i].activeSelf)
			{
				return pool[i];
			}
		}
		
		//If we are here we have run out of pool members
		ExpandPool(1);
		return GetNextInactive();
	}
	
	private void ExpandPool(int expandBy)
	{
		for (int i = 0; i < expandBy; i++)
		{
			GameObject item = GameObject.Instantiate(prefab, Vector3.zero, prefab.transform.localRotation) as GameObject;
			item.name = baseName + '-' + (poolSize + i).ToString();
			item.transform.parent = parentTransform;
			item.SetActive(false);
			pool.Add(item);
		}
		
		poolSize += expandBy;
		Debug.Log("[BasicPool]::Pool for " + prefab.name + " expanded by " + expandBy);
	}
	
	public void Shrink(int shrinkBy)
	{
		for (int i = 0; i < shrinkBy; i++)
		{
			if (pool.Count > 0)
			{
				GameObject.Destroy(pool[0]);
				pool.RemoveAt(0);
			}
			else
			{
				Debug.Log("[BasicPool]::No more items left to remove in pool");
				break;
			}
		}
	}
	
	public void ResetPoolSize()
	{
		for (int i = pool.Count - 1; i > initPoolSize; i--)
		{
			GameObject.Destroy(pool[i]);
			pool.RemoveAt(i);
		}
	}
}