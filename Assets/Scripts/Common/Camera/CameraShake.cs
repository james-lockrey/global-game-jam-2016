﻿using UnityEngine;
using System.Collections;

public class CameraShake : MonoBehaviour
{
	// CACHED COMPONENTS
	[HideInInspector]
	public new Transform transform;

	private bool isShaking = false;
	private float shakeIntensity;
	private const float SHAKE_DECAY = 0.025f;
	private Vector3 movement;

	#region MonoBehaviour Lifecycle
	void Awake()
	{
		transform = gameObject.transform;
	}
	#endregion

	public void Shake(float intensity)
	{
		shakeIntensity = Mathf.Max(shakeIntensity, intensity);
		
		if (!isShaking)
		{
			isShaking = true;
			StartCoroutine(CR_Shake());
		}
	}
	
	private IEnumerator CR_Shake()
	{
		Vector3 mainCamStartPos = transform.position;
		
		while (shakeIntensity > 0f)
		{
			movement = Random.insideUnitCircle * shakeIntensity;
			transform.position = mainCamStartPos + movement;
			
			// For some reason, changing the rotation causes draw calls to massively spike (100 draw calls for the duration of the shake)
			//_transform.localRotation = new Quaternion(rotation.x + Random.Range(-shakeIntensity, shakeIntensity) * 0.02f,
			//    rotation.y + Random.Range(-shakeIntensity, shakeIntensity) * 0.02f,
			//    rotation.z + Random.Range(-shakeIntensity, shakeIntensity) * 0.02f,
			//    rotation.w + Random.Range(-shakeIntensity, shakeIntensity) * 0.02f);
			
			shakeIntensity -= SHAKE_DECAY;
			yield return null;
		}
		
		shakeIntensity = 0f;
		movement = Vector3.zero;
		transform.position = mainCamStartPos;
		
		isShaking = false;
	}
}
