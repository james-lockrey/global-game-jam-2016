﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class DelayPlayOnAwake : MonoBehaviour
{
    // CACHED COMPONENTS
    private AudioSource _audioSource;

    public float delay = 0.5f;

    #region MonoBehaviour Lifecycle
    void Awake()
    {
        _audioSource = GetComponent<AudioSource>();

        _audioSource.playOnAwake = false;
        _audioSource.Stop();
    }

    void Start()
    {
        StartCoroutine(CR_DelayPlay(delay));
    }
    #endregion

    private IEnumerator CR_DelayPlay(float delay)
    {
        yield return new WaitForSeconds(0.5f);
        _audioSource.Play();
        Destroy(this);
    }
}
