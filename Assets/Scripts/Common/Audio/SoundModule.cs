﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class SoundModule : MonoBehaviour
{
	public enum PlayMode
	{
		Random,
		RandomNoRepeats,
		Ordered
	}
	
	public List<AudioClip> clips = new List<AudioClip>();
	public PlayMode playMode;
	
	private int lastClipIndex = 0;
	
	#region Properties
	public AudioClip Clip
	{
		get
		{
			switch (playMode)
			{
			case PlayMode.Random:
				return GetRandomClip(true);
			case PlayMode.RandomNoRepeats:
				return GetRandomClip(false);
			case PlayMode.Ordered:
				return GetOrderedClip();
			}
			
			return GetRandomClip(true);
		}
	}
	#endregion
	
	private AudioClip GetRandomClip(bool allowRepeats)
	{
		if (clips.Count == 0)
			return null;
		else if (clips.Count == 1)
			return clips[0];
		
		if (allowRepeats)
		{
			lastClipIndex = Random.Range(0, clips.Count);
			return clips[lastClipIndex];
		}
		else
		{
			List<AudioClip> possibleChoices = clips.Where((x, i) => i != lastClipIndex).ToList();
			AudioClip chosen = possibleChoices[Random.Range(0, possibleChoices.Count)];
			lastClipIndex = clips.IndexOf(chosen);
			return chosen;
		}
	}
	
	private AudioClip GetOrderedClip()
	{
		if (clips.Count == 0)
			return null;
		
		AudioClip toPlay = clips[lastClipIndex];
		lastClipIndex = (lastClipIndex + 1) % clips.Count;
		
		return toPlay;
	}
}