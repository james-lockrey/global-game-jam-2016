﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class AudioEngine : MonoBehaviour
{
    private static AudioEngine instance = null;
    public static AudioEngine Instance { get { return instance; } }

    // CACHED COMPONENTS
    private AudioSource _audioSource;

    [SerializeField]
    private GameObject globalSoundCollection;

    private Dictionary<string, SoundModule> globalSounds = new Dictionary<string, SoundModule>();

    public SnapshotTransition transitionIn;
    public SnapshotTransition transitionOut;
    private bool isFading;

    #region MonoBehaviour Lifecycle
    void Awake()
    {
        // Singleton bits
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        else
        {
            instance = this;
        }

        _audioSource = GetComponent<AudioSource>();

    }

    void Start()
    {
        foreach(Transform child in globalSoundCollection.transform)
            globalSounds.Add(child.gameObject.name, child.GetComponent<SoundModule>());
    }

    void OnDestroy()
    {

    }
    #endregion

    #region Events
    #endregion

    public void PlayOneShot(AudioClip clip)
    {
        _audioSource.PlayOneShot(clip);
    }

    public void PlayOneShot(string id)
    {
        if(globalSounds.ContainsKey(id))
            _audioSource.PlayOneShot(globalSounds[id].Clip);
    }
}