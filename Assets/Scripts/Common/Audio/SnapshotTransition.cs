﻿using UnityEngine;
using UnityEngine.Audio;
using System.Collections.Generic;

public class SnapshotTransition : MonoBehaviour
{
	[SerializeField] private List<AudioMixerSnapshot> snapshots = new List<AudioMixerSnapshot>();
	[SerializeField] private float transitionDuration;
	
	#region MonoBehaviour Lifecycle
	void OnTriggerEnter(Collider other)
	{
		Transition();
	}
	#endregion
	
	public void Transition()
	{
		foreach (AudioMixerSnapshot snapshot in snapshots)
			snapshot.TransitionTo (transitionDuration);
	}
}