﻿using UnityEngine;
using System.Collections;

public class InstantiateOnEnable : MonoBehaviour
{
	// CACHED COMPONENTS
	private Transform _transform;
	
	public GameObject prefab;
	
	#region MonoBehaviour Lifecycle
	void Awake()
	{
		_transform = GetComponent<Transform>();
	}
	
	void OnEnable()
	{
		GameObject child = Instantiate(prefab, _transform.position, prefab.transform.rotation) as GameObject;
		Transform parent = _transform.parent;
		if(parent != null)
			child.transform.parent = parent;
	}
	#endregion
}