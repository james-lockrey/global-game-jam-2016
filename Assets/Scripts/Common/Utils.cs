﻿using System;
using System.Collections.Generic;
using UnityEngine;

public enum CollisionLayer
{
	Nothing = -1,
	Default = 0,
	TransparentFX = 1,
	IgnoreRaycast = 2,
	UI = 5,
}

public enum LanguageID
{
	EN,
	DE,
	ES,
	FR,
	IT,
	PO,
	RU
}

public static class Utils
{
	public static int GetLayerMask(CollisionLayer layer)
	{
		return 1 << (int)layer;
	}
	
	public static Vector2 RoundVector(Vector2 vector)
	{
		return new Vector2(Mathf.Round(vector.x), Mathf.Round(vector.y));
	}
	
	public static Vector3 RoundVector(Vector3 vector)
	{
		return new Vector3(Mathf.Round(vector.x), Mathf.Round(vector.y), Mathf.Round(vector.z));
	}
	
	public static bool IsInsideGrid(Vector2 value, int width, int height)
	{
		return !(value.x < 0 || value.x >= width ||
		         value.y < 0 || value.y >= height);
	}

	public static float Map(this float value, float from1, float to1, float from2, float to2)
	{
		return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
	}
	
	public static string SecondsToTimeString(float seconds, string format = "hh:mm:ss")
	{
		TimeSpan ts = TimeSpan.FromSeconds(seconds);
		
		if (format.ToLower() == "mm:ss")
			return String.Format("{0:00}:{1:00}", ts.Minutes, ts.Seconds);
		else
			return String.Format("{0:00}:{1:00}:{2:00}", ts.Hours, ts.Minutes, ts.Seconds);
		
	}
	
	public static float Truncate(float value, int digits)
	{
		double mult = Math.Pow(10.0, digits);
		double result = Math.Truncate(mult * value) / mult;
		return (float)result;
	}
	
	public static string ConvertToHex(int num, int characters)
	{
		return num.ToString("X" + characters.ToString());
	}
	
	public static LanguageID StringtoLanguageId(string key)
	{
		key = key.ToLower();
		if (key == "english" || key == "en")
			return LanguageID.EN;
		else if (key == "french" || key == "fr")
			return LanguageID.FR;
		else if (key == "german" || key == "de")
			return LanguageID.DE;
		else if (key == "spanish" || key == "es")
			return LanguageID.ES;
		else if (key == "italian" || key == "it")
			return LanguageID.IT;
		else if (key == "portuguese" || key == "po")
			return LanguageID.PO;
		else if (key == "russian" || key == "ru")
			return LanguageID.RU;
		
		throw new KeyNotFoundException();
	}
	
	public static string LanguageIdToString(LanguageID id)
	{
		switch (id)
		{
		case LanguageID.EN:
			return "English";
		case LanguageID.FR:
			return "French";
		case LanguageID.DE:
			return "German";
		case LanguageID.ES:
			return "Spanish";
		case LanguageID.IT:
			return "Italian";
		case LanguageID.PO:
			return "Portuguese";
		case LanguageID.RU:
			return "Russian";
		default:
			throw new KeyNotFoundException();
		}
	}
	
	public static string LanguageCodeToString(string code)
	{
		code = code.ToLower();
		
		switch (code)
		{
		case "en":
			return "English";
		case "fr":
			return "French";
		case "de":
			return "German";
		case "es":
			return "Spanish";
		case "it":
			return "Italian";
		case "po":
			return "Portuguese";
		case "ru":
			return "Russian";
		default:
			throw new KeyNotFoundException();
		}
	}
	
	/*
	public static <T> FindComponentInParent(GameObject origin)
	{
		Transform child = origin.transform;
		while(child != null)
		{
			T target = child.GetComponent<T>();
			if(target != null)
				return target;
			else
				child = child.parent;
		}

		return null;
	}
	*/
}
