﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public abstract class ISaveManager
{
    public enum SaveType
    {
        PlayerPrefs,
        Disk,
        Cloud
    }

    public enum SaveEncoding
    {
        Text,
        Bytes
    }

    // Which locations are we attempting to load / save from / to?
    protected List<SaveType> SaveLocations = new List<SaveType>();
    protected SaveEncoding Encoding;
    protected bool IsDirty;

    public delegate void DataLoadedDelegate();
    public static event DataLoadedDelegate OnLoad = delegate { };
    public delegate void DataSavedDelegate();
    public static event DataSavedDelegate OnSave = delegate { };

    #region Properties
    public string SaveKey { get; protected set; }
    public string RelativeFilePath { get; protected set; }
    public string SaveFileName { get; protected set; }
    #endregion

    #region MonoBehaviour Lifecycle
    protected virtual void Awake()
    {

    }
    #endregion

    public ISaveManager()
    {
        IsDirty = false;
    }

    protected void AddSaveLocation(SaveType type)
    {
        if(!SaveLocations.Contains(type))
            SaveLocations.Add(type);
    }

    #region Load / Save
    public virtual void Load()
    {
        // Order of execution defines order of preference
        if (SaveLocations.Contains(SaveType.PlayerPrefs))
            LoadDataFromPlayerPrefs();
        else if (SaveLocations.Contains(SaveType.Disk))
            LoadDataFromDisk();

        //if (SaveLocations.Contains(SaveType.Cloud))
        //    LoadDataFromCloud();

        OnLoad();
    }

    protected void LoadDataFromPlayerPrefs()
    {
        // FOR FIRST TIME SETUP ONLY
        if (!PlayerPrefs.HasKey(SaveKey))
        {
            // TODO: put code to upgrade from previous save data encoding versions here

            PlayerPrefs.DeleteAll();
            switch (Encoding)
            {
                case SaveEncoding.Bytes:
                    PlayerPrefs.SetString(SaveKey, BitConverter.ToString(this.ToBytes()));
                    break;
                case SaveEncoding.Text:
                    PlayerPrefs.SetString(SaveKey, this.ToString());
                    break;
            }

            PlayerPrefs.Save();

            // Check to see if the user has save data in the cloud
            //LoadDataFromCloud();
        }

        switch(Encoding)
        {
            case SaveEncoding.Bytes:
                Debug.Log("[ISaveManager]::Loading save data from PlayerPrefs (Encoded as bytes)");
                string data = PlayerPrefs.GetString(SaveKey);
                // TODO: bytes encoding not yet supported
                LoadFromBytes(System.Text.Encoding.UTF8.GetBytes(data));
                break;
            case SaveEncoding.Text:
                Debug.Log("[ISaveManager]::Loading save data from PlayerPrefs (Encoded as text)");
                LoadFromString(PlayerPrefs.GetString(SaveKey));
                break;
            default:
                Debug.LogError("[ISaveManager]::No save data encoding was specified!");
                break;
        }
    }

    protected void LoadDataFromDisk()
    {
#if (!UNITY_WEBPLAYER) // Loading from disc not supported in webplayer
        // FOR FIRST TIME SETUP ONLY
        if (!File.Exists(RelativeFilePath + SaveFileName))
            SaveDataToDisk();

        switch (Encoding)
        {
            case SaveEncoding.Bytes:
                Debug.Log("[ISaveManager]::Loading save data from Disk (Encoded as bytes)");
                LoadFromBytes(File.ReadAllBytes(RelativeFilePath + SaveFileName));
                break;
            case SaveEncoding.Text:
                Debug.Log("[ISaveManager]::Loading save data from Disk (Encoded as text)");
                LoadFromString(File.ReadAllText(RelativeFilePath + SaveFileName));
                break;
            default:
                Debug.LogError("[ISaveManager]::No save data encoding was specified! Attempting to load as text...");
                LoadFromString(File.ReadAllText(RelativeFilePath + SaveFileName));
                break;
        }
#endif
    }

    protected abstract void LoadFromString(string s);

    protected void LoadFromBytes(byte[] b)
    {
        LoadFromString(System.Text.Encoding.UTF8.GetString(b));
    }

    public void Save()
    {
        if (IsDirty)
        {
            // Saves to one or the other, PlayerPrefs preferred
            if (SaveLocations.Contains(SaveType.PlayerPrefs))
                SaveDataToPlayerPrefs();
            else if (SaveLocations.Contains(SaveType.Disk))
                SaveDataToDisk();

            IsDirty = false;
            OnSave();
        }
        else
        {
            Debug.Log("[ISaveManager]::No changes to be saved.");
        }
    }

    protected virtual void SaveDataToPlayerPrefs()
    {
        switch(Encoding)
        {
            case SaveEncoding.Bytes:
                Debug.Log("[ISaveManager]::Data saved to PlayerPrefs (Encoded as bytes)");
                PlayerPrefs.SetString(SaveKey, BitConverter.ToString(this.ToBytes()));
                break;
            case SaveEncoding.Text:
                Debug.Log("[ISaveManager]::Data saved to PlayerPrefs (Encoded as text)");
                PlayerPrefs.SetString(SaveKey, this.ToString());
                break;
        }

        PlayerPrefs.Save();
    }

    protected virtual void SaveDataToDisk()
    {
#if (!UNITY_WEBPLAYER)
        string path = RelativeFilePath + SaveFileName;
        switch (Encoding)
        {
            case SaveEncoding.Bytes:
                File.WriteAllText(path, BitConverter.ToString(this.ToBytes()));
                Debug.Log("[ISaveManager]::Data saved to disk (Encoded as text)");
                break;
            case SaveEncoding.Text:
                File.WriteAllText(path, this.ToString());
                Debug.Log("[ISaveManager]::Data saved to disk (Encoded as text)");
                break;
        }
#endif
    }

    public virtual void MergeWith(ISaveManager other) { }

    // Custom save data encoding method
    public new abstract string ToString();

    public byte[] ToBytes()
    {
        return System.Text.Encoding.UTF8.GetBytes(this.ToString());
    }
    #endregion
}