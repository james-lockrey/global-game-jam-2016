﻿using Common.Save.JSON;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public class SaveManager : ISaveManager
{
    // Settings
    private bool isSoundEnabled = true;
    private LanguageID currentLanguage = LanguageID.EN;
    private bool hasRatedApp = false;

    // Player stats
    private int totalEarnings = 0;

    #region Properties
    // Settings
    public bool IsSoundEnabled
    {
        get { return isSoundEnabled; }
        set { isSoundEnabled = value; IsDirty = true; }
    }
    public LanguageID CurrentLanguage
    {
        get { return currentLanguage; }
        set { currentLanguage = value; IsDirty = true; }
    }
    public bool HasRatedApp
    {
        get { return hasRatedApp; }
        set { hasRatedApp = value; IsDirty = true; }
    }
    // Player stats
    public int TotalEarnings
    {
        get { return totalEarnings; }
        set { totalEarnings = value; IsDirty = true; }
    }
    #endregion

    public SaveManager()
        : base()
    {
        SaveKey = "ctg-sushisamurai-v1";
        RelativeFilePath = "Assets/Resources/";
        SaveFileName = "sushi-samurai.sav";

        // Add save locations
        AddSaveLocation(SaveType.PlayerPrefs);
        //AddSaveLocation(SaveType.Cloud);

        // Set encoding preference
        Encoding = SaveEncoding.Text;

        if (Core.Instance.Reset)
            PlayerPrefs.DeleteAll();
    }

    #region Load / Save
    public static SaveManager FromString(string data)
    {
        SaveManager save = new SaveManager();
        save.LoadFromString(data);
        return save;
    }

    public static SaveManager FromBytes(byte[] bytes)
    {
        SaveManager save = new SaveManager();
        save.LoadFromBytes(bytes);
        return save;
    }

    public bool MergeWith(SaveManager other)
    {
        return false;
    }

    protected override void LoadFromString(string s)
    {
        JSONObject saveData = JSONObject.Parse(s);

        if (!saveData.ContainsKey(SaveKey))
        {
            Debug.LogError("[SaveManager]::Error parsing data from: " + s);
            return;
        }

        // General settings
        isSoundEnabled = saveData.GetBoolean("set-sound");
        currentLanguage = Utils.StringtoLanguageId(saveData.GetString("set-lang"));
        hasRatedApp = saveData.GetBoolean("set-rating");

        // Player stats
        totalEarnings = (int)saveData.GetNumber("stats-earnings");
    }

    public override string ToString()
    {
        JSONObject saveData = new JSONObject();

        saveData.Add(SaveKey, 1);
        // General settings
        saveData.Add("set-sound", isSoundEnabled);
        saveData.Add("set-lang", currentLanguage.ToString());
        saveData.Add("set-rating", hasRatedApp);
        //Player stats
        saveData.Add("stats-earnings", totalEarnings);

        return saveData.ToString();
    }
    #endregion
}
