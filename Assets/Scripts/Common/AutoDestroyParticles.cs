﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(ParticleSystem))]
public class AutoDestroyParticles : MonoBehaviour
{
	// CACHED COMPONENTS
	private ParticleSystem _particleSystem;
	
	#region MonoBehaviour Lifecycle
	void Awake()
	{
		_particleSystem = GetComponent<ParticleSystem>();
	}
	
	void OnDisable()
	{
		Destroy(gameObject);
	}
	#endregion
	
	void Update()
	{
		if(!_particleSystem.IsAlive())
		{
			Destroy(gameObject);
		}
	}
}