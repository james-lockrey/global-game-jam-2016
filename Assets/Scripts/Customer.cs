﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Customer : MonoBehaviour
{
	// CACHED COMPONENTS
	private Animator _animator;

	public Image speechBubble;
    public Image requestedItem;

	public Image platedFood;
	public Image topOfPlateFood;

	[Header("Sounds")]
	public SoundModule placeOrderSounds;
	public SoundModule enjoyFoodSounds;
	public SoundModule hateFoodSounds;

	#region Properties
	public bool IsEating { get; private set; }
	#endregion

    #region MonoBehaviour Lifecycle
    void Awake()
    {
		_animator = GetComponent<Animator>();

		IsEating = false;
        gameObject.SetActive(false);

        BarManager.OnSequenceStarted += OnSequenceStarted;
    }

	void OnEnable()
	{
		speechBubble.gameObject.SetActive(false);
		platedFood.gameObject.SetActive(false);

		StartCoroutine(CR_FixPopping());
	}
    #endregion

    #region Events
    private void OnSequenceStarted(List<MiniGame> newSequence)
    {
        speechBubble.gameObject.SetActive(false);
    }
    #endregion

	private IEnumerator CR_FixPopping()
	{
		Image myImage = GetComponent<Image>();
		myImage.enabled = false;
		yield return new WaitForEndOfFrame();
		yield return new WaitForEndOfFrame();
		myImage.enabled = true;
	}

    public void Spawn()
    {
        gameObject.SetActive(true);
		_animator.SetTrigger("SpawnTrig");
    }

    public void RequestOrder(Recipe order)
    {
        StartCoroutine(CR_RequestOrder(order.ExampleSprite));
    }

    private IEnumerator CR_RequestOrder(Sprite example)
    {
		SoundKit.instance.playOneShot(placeOrderSounds.Clip);

        yield return new WaitForSeconds(0.5f);

        requestedItem.sprite = example;
        speechBubble.gameObject.SetActive(true);
    }

	public void ServeFood(Recipe order, bool isSuccess)
	{
		IsEating = true;

		StartCoroutine(CR_ConsumeFood(order, isSuccess));
	}

	private IEnumerator CR_ConsumeFood(Recipe order, bool isSuccess)
	{
		platedFood.gameObject.SetActive(true);
		topOfPlateFood.sprite = isSuccess ? order.ExampleSprite : order.FailedSprite;

		yield return new WaitForSeconds(2f);

		if(isSuccess)
		{
			if(enjoyFoodSounds != null)
				SoundKit.instance.playOneShot(enjoyFoodSounds.Clip);

			_animator.SetFloat ("ReactionFloat", 0.5f);
		}
		else
		{
			if(hateFoodSounds != null)
				SoundKit.instance.playOneShot(hateFoodSounds.Clip);

			_animator.SetFloat ("ReactionFloat", -0.5f);
		}

		_animator.SetTrigger("EatingTrig");

		yield return new WaitForEndOfFrame();

		platedFood.gameObject.SetActive(false);

		yield return new WaitForSeconds(3.33f);

		IsEating = false;
	}
}
